// headers

#include <stdio.h>

int main(void)
{
    // function declarations
    void PrintCUDADeviceProperties(void);

    // code
    PrintCUDADeviceProperties();
}

void PrintCUDADeviceProperties(void)
{
    // function declaration
    int ConvertSMVersionNumberToCores(int, int);

    // code
    printf("CUDA INFORMATION :\n");
    printf("----------------------------------------------------------------------------------\n");

    cudaError_t ret_cuda_rt;
    int dev_count;
    ret_cuda_rt = cudaGetDeviceCount(&dev_count);

    if(ret_cuda_rt != cudaSuccess)
    {
        printf("CUDA runtime API error - cudaGetDeviceCount() failed due to %s. Exiting now...\n", 
        cudaGetErrorString(ret_cuda_rt));
    }
    else
    if(dev_count == 0)
    {
        printf("There is no CUDA supported device on this system. Exiting now...\n");
        return;
    }
    else
    {
        printf("total number of CUDA supporting GPU device/devices on this system: %d\n", dev_count);

        for(int i=0; i < dev_count; i++)
        {
            cudaDeviceProp dev_prop;
            int driverVersion = 0, runtimeVersion = 0;

            ret_cuda_rt = cudaGetDeviceProperties(&dev_prop, i);
            if(ret_cuda_rt != cudaSuccess)
            {
                printf("%s in %s at line %d\n", cudaGetErrorString(ret_cuda_rt), __FILE__, __LINE__);
                return;
            }

            printf("\n");
            cudaDriverGetVersion(&driverVersion);
            cudaRuntimeGetVersion(&runtimeVersion);
            printf("****** CUDA driver and runtime information ******\n");
            printf("CUDA driver version: %d.%d\n", driverVersion/ 1000, (driverVersion % 100) / 10);
            printf("CUDA runtime version: %d.%d\n", runtimeVersion / 1000, (runtimeVersion % 100) / 10);

            printf("****** GPU Device General Information ******\n");

            printf("GPU device number: %d\n", i);
            printf("GPU device name: %s\n", dev_prop.name);
            printf("GPU device compute capability: %d.%d\n", dev_prop.major, dev_prop.minor);
            printf("GPU device clock rate: %d\n", dev_prop.clockRate);
            printf("GPU device type: \n");
            if(dev_prop.integrated)
                printf("Integrated (On-Board)\n");
            else
                printf("Discrete (Card)\n");
            printf("\n");

            printf("***** GPU device memory inforamtion ******\n");
            printf("GPU device total memory: %.0f GB = %.0f MB = %llu Bytes\n", 
            ((float)dev_prop.totalGlobalMem / 1048576.0f) / 1024.0f, (float)dev_prop.totalGlobalMem / 1048576.0f, 
            (unsigned long long) dev_prop.totalGlobalMem);

            printf("GPU device available memory: %lu Bytes\n", (unsigned long)dev_prop.totalConstMem);
            printf("GPU device host memory mapping capability: ");
            if(dev_prop.canMapHostMemory)
            {
                printf("Yes (Can map host memory to device memory)\n");
            }
            else
            {
                printf("No (Can not map host memory to device memory)\n");
            }
            printf("\n");
            printf("****** GPU device multiprocessor information\n");

            printf("GPU device number of SMProcessors : %d\n", dev_prop.multiProcessorCount);

            printf("GPU device number of cores per SMProcessors: %d\n",
            ConvertSMVersionNumberToCores(dev_prop.major, dev_prop.minor));

            printf("GPU device total number of cores : %d\n",
            ConvertSMVersionNumberToCores(dev_prop.major, dev_prop.minor) * dev_prop.multiProcessorCount);

            printf("GPU device shared memory per SMProcessor : %lu\n", (unsigned long)dev_prop.sharedMemPerBlock);
            printf("GPU device number of registeres per SMProcessor : %d\n", dev_prop.regsPerBlock);
            printf("\n");

            printf("****** GPU device thread information ******\n");
            printf("GPU device maximum number of threads per SMProcessor : %d\n", 
            dev_prop.maxThreadsPerMultiProcessor);

            printf("GPU device maximum number of threads per block: %d\n", dev_prop.maxThreadsPerBlock);
            printf("GPU device threads in Warp: %d\n", dev_prop.warpSize);
            printf("GPU device maximum thread dimentions : (%d, %d, %d)\n", 
            dev_prop.maxThreadsDim[0], dev_prop.maxThreadsDim[1],
            dev_prop.maxThreadsDim[2]);
            printf("GPU device maximum grid dimentions: (%d, %d, %d)\n",
            dev_prop.maxGridSize[0], dev_prop.maxGridSize[1],
            dev_prop.maxGridSize[2]);
            printf("\n");

            printf("****** GPU device driver information ******\n");
            printf("GPU device has ECC support : %s\n",
            dev_prop.ECCEnabled ? "Enabled" : "Disabled");
            #if defined(WIN32) || defined (_WIN32) || defined(WIN64) || defined(_WIN64)
            printf("GPU device CUDA driver mode (TCC or WDDM) : %s\n",
            dev_prop.tccDriver ? "TCC (Tesla computer cluster driver)" : 
            "WDDM (Windows display driver model)");
            #endif
            printf("\n");
        }        
    }
}
    
int ConvertSMVersionNumberToCores(int major, int minor)
{
    // Defines for GPU architecture types 
    // (using the SM version to determine the no of cores per SM)
    typedef struct
    {
        // 0xMm (hexadecimal notation), M = SM major version, and m = SM
        // minor version

        int SM;
        int Cores;
    }sSMtoCores;

    sSMtoCores nGpuArchCoresPerSM[] = 
    {
        {0x20, 32}, // Fermi generation (SM 2.0) GF100 class
        {0x21, 48}, // Fermi generation (SM 2.1) GF10x class
        {0x30, 192}, // Kepler generation (SM 3.0) GK10x class
        {0x32, 192}, // Kepler generation (SM 3.2) GK10x class
        {0x35, 192}, // Kepler generation (SM 3.5) GK11x class
        {0x37, 192}, // Kepler generation (SM 3.7) GK21x class
        {0x50, 128}, // Maxwell generation (SM 5.0) GM10x class
        {0x52, 128}, // Maxwell generation (SM 5.2) GM20x class
        {0x53, 128}, // Maxwell generation (SM 5.3) GM20x class
        {0x60, 64}, // Pascal generation (SM 6.0) GP100 class
        {0x61, 128}, // Pascal generation (SM 6.1) GM10x class
        {0x62, 128}, // Pascal generation (SM 6.2) GM10x class
        {-1, -1}
    };

    int index = 0;
    while(nGpuArchCoresPerSM[index].SM != -1)
    {
        if(nGpuArchCoresPerSM[index].SM == ((major << 4) + minor))
        {
            return nGpuArchCoresPerSM[index].Cores;
        }

        index++;
    }

    // if we don't find the values, we default use the previous one to run properly
    printf("MapSMtoCores for SM %d.%d is undefined. Default to use %d Cores/SM\n", major, minor, nGpuArchCoresPerSM[index - 1].Cores);
    return (nGpuArchCoresPerSM[index - 1].Cores);
}

