#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#define EXPORT __declspec(dllexport)
#else
#define EXPORT __attribute__ ((visibility("default")))
#endif

class StopWatchInterface
{
    public:
        StopWatchInterface() {};
        virtual ~StopWatchInterface() {};
        virtual void start() = 0;   // start time measurement
        virtual void stop() = 0;
        virtual void reset() = 0;   // reset time counter to zero
        virtual float getTime() = 0;
        virtual float getAverageTime() = 0;
};

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#define WINDOWS_LEAN_AND_MEAN
#include <Windows.h>

#undef min
#undef max

// Windows specific implementation of StopWatch
class StopWatchWin : public  StopWatchInterface
{
    private:
    // member variables

    // start of measurement
    LARGE_INTEGER start_time;

    // End of measurement
    LARGE_INTEGER end_time;

    // time difference between the last start and stop 
    float diff_time;

    // Total time difference between starts and stops
    float total_time;

    // flag if the stop watch is running
    bool running;

    // number of times clock has been started and stopped to allow averaging
    int clock_sessions;

    // tick frequency
    double freq;

    // flag if the frequency has been set
    bool freq_set;

    public :
    // default public constructor
    StopWatchWin() : start_time(), end_time(), diff_time(0.0f), total_time(0.0f),
                     running(false), clock_sessions(0), freq(0), freq_set(false)
    {
        if(!freq_set)
        {
            // helper variable
            LARGE_INTEGER temp;

            // get the tick frequency from the OS
            QueryPerformanceFrequency((LARGE_INTEGER *)&temp);

            // convert to type in which it is needed
            freq = ((double) temp.QuadPart) / 1000.0;

            // remember query
            freq_set = true;
        }
    };

    // Destructor
    ~StopWatchWin() { };

    public:
    // start time measurement
    inline void start();

    // stop time measurement
    inline void stop();

    // reset time counters to zero
    inline void reset();

    inline float getTime();

    inline float getAverageTime();
};

inline void StopWatchWin::start()
{
    QueryPerformanceCounter((LARGE_INTEGER *) &start_time);
    running = true;
}

inline void StopWatchWin::stop()
{
    QueryPerformanceCounter((LARGE_INTEGER *) &end_time);
    diff_time = (float)
    (((double) end_time.QuadPart - (double) start_time.QuadPart) / freq);
    total_time += diff_time;
    clock_sessions++;
    running = false;
}

inline void StopWatchWin::reset()
{
    diff_time = 0;
    total_time = 0;
    clock_sessions = 0;

    if(running)
    {
        QueryPerformanceCounter((LARGE_INTEGER *) &start_time);
    }
}

inline float StopWatchWin::getTime()
{
    // return the total time to date
    float retval = total_time;

    if(running)
    {
        LARGE_INTEGER temp;
        QueryPerformanceCounter((LARGE_INTEGER *) &temp);
        retval += (float)(((double)(temp.QuadPart - start_time.QuadPart)) / freq);
    }

    return retval;
}

inline float StopWatchWin::getAverageTime()
{
    return (clock_sessions > 0) ? (total_time/clock_sessions) : 0.0f;
}
#else
// declaration for StopWatch on Linux and Mac OSx
// includes, system
#include <ctime>
#include <sys/time.h>

// Linux specific implementation of StopWatch
class StopWatchLinux : public StopWatchInterface
{
    private:
    // member variables

    // start of measurement
    struct timeval start_time;

    // time difference between the last start and stop 
    float diff_time;

    // Total time difference between starts and stops
    float total_time;

    // flag if the stop watch is running
    bool running;

    // number of times clock has been started and stopped to allow averaging
    int clock_sessions;

    public:
    // constructor, default
    StopWatchLinux() : start_time(), diff_time(0.0), total_time(0.0),
                        running(false), clock_sessions(0)
                        {};
    
    // destructor
    virtual ~StopWatchLinux() {};

    // start time measurement
    inline void start();

    // stop time measurement
    inline void stop();

    // reset time counters to zero
    inline void reset();

    inline float getTime();

    inline float getAverageTime();

    inline float getDiffTime();
};

inline void StopWatchLinux::start()
{
    gettimeofday(&start_time, 0);
    running = true;
}

inline void StopWatchLinux::stop()
{
    diff_time = getDiffTime();
    total_time += diff_time;
    running = false;
    clock_sessions++;
}

inline void StopWatchLinux::reset()
{
    diff_time = 0;
    total_time = 0;
    clock_sessions = 0;

    if(running)
    {
        gettimeofday(&start_time, 0);
    }
}

inline float StopWatchLinux::getTime()
{
    // return the total time to date
    float retval = total_time;

    if(running)
    {
        retval += getDiffTime();
    }

    return retval;
}

inline float StopWatchLinux::getAverageTime()
{
    return (clock_sessions > 0) ? (total_time / clock_sessions) : 0.0f;
}

inline float StopWatchLinux::getDiffTime()
{
    struct timeval t_time;
    gettimeofday(&t_time, 0);

    // time difference in milli-seconds
    return (float)(1000.0 * (t_time.tv_sec - start_time.tv_sec) +
    (0.001 * (t_time.tv_usec - start_time.tv_usec)));
}
#endif // WIN32

EXPORT inline bool sdkCreateTimer(StopWatchInterface **timer_interface)
{
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
    *timer_interface = (StopWatchInterface *)new StopWatchWin();
#else
    *timer_interface = (StopWatchInterface *)new StopWatchLinux();
#endif

    return (*timer_interface != NULL) ? true : false;
}

EXPORT inline bool sdkDeleteTimer(StopWatchInterface **timer_interface)
{
    if(*timer_interface)
    {
        delete *timer_interface;
        *timer_interface = NULL;
    }

    return true;
}

EXPORT inline bool sdkStartTimer(StopWatchInterface **timer_interface)
{
    if(*timer_interface)
    {
        (*timer_interface)->start();
    }

    return true;
}

EXPORT inline bool sdkStopTimer(StopWatchInterface **timer_interface)
{
    if(*timer_interface)
    {
        (*timer_interface)->stop();
    }

    return true;
}

EXPORT inline bool sdkResetTimer(StopWatchInterface **timer_interface)
{
    if(*timer_interface)
    {
        (*timer_interface)->reset();
    }

    return true;
}

EXPORT inline float sdkGetAverageTimerValue(StopWatchInterface **timer_interface)
{
    if(*timer_interface)
    {
        return (*timer_interface)->getAverageTime();
    }
    else
    {
        return 0.0f;
    }
}

EXPORT inline float sdkGetTimerValue(StopWatchInterface **timer_interface)
{
    if(*timer_interface)
    {
        return (*timer_interface)->getTime();
    }
    else
    {
        return 0.0f;
    }
}

