#pragma once

#define draw_circle(numPoints,delta,r,g,b)		\
for (int i = 0; i < numPoints; i++)				\
{												\
	float angle = (2.0 * M_PI *i) / numPoints;	\
	float x = cos(angle);						\
	float y = sin(angle);						\
	if (x > 0)									\
		x = x - fabs(x) * delta;				\
	else                                        \
		if (x < 0)								\
			x = x + fabs(x) * delta;			\
	if (y > 0)									\
		y = y - fabs(y) * delta;				\
	else                                        \
		if (y < 0)								\
			y = y + fabs(y) * delta;			\
	glVertex3f(x, y, 0.0f);						\
	glColor3f(r,g,b);							\
}												\

