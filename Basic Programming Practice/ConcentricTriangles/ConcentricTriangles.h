#pragma once

#define draw_line(x,y,init_x,init_y,r,g,b, deltaX, deltaY)	\
while(x <= init_x && y >= init_y)							\
{															\
	glVertex3f(x, y, 0.0f);									\
    if(y <= 1.0f && y > -1.0f) 								\
	y -= deltaY;											\
	x += deltaX; 											\
	glColor3f(r, g, b);										\
}															\
