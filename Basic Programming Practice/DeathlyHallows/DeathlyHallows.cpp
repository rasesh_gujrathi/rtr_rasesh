#define _USE_MATH_DEFINES 1

#include <Windows.h>
#include <assert.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <math.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// Global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// open-gl rendering context
HWND gHwnd = NULL;
DWORD dw_style;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbFullScreen = false;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void toggleFullScreen(void);
void resize(int width, int height);
void uninitialize(void);
void display(void);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	int initialize(void);
	int iRet = 0;
	bool bDone = false;

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("opengl - deathly hallow"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS |
		WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	assert(hwnd);

	iRet = initialize();

	if (iRet < 0)
	{
		// error handling
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// call update
			}
			display();
		}
	}

	return((int)msg.wParam);
}

int width = 0, height = 0;

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void toggleFullScreen(void);

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		gHwnd = hwnd;
		break;
	case WM_SETFOCUS: gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: gbActiveWindow = false;
		break;
	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(width, height);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			toggleFullScreen();
			break;
		}
		break;
	case WM_ERASEBKGND:
		return (0);
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void toggleFullScreen(void)
{
	MONITORINFO mI;

	if (gbFullScreen == false)
	{
		dw_style = GetWindowLong(gHwnd, GWL_STYLE);

		if (dw_style & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev))
			{
				HMONITOR hMonitor = MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY);
				if (hMonitor)
				{
					mI.cbSize = sizeof(MONITORINFO);
					if (GetMonitorInfo(hMonitor, &mI))
					{
						SetWindowLong(gHwnd, GWL_STYLE, dw_style &~WS_OVERLAPPEDWINDOW);

						SetWindowPos(gHwnd,
							HWND_TOP,
							mI.rcMonitor.left,
							mI.rcMonitor.top,
							mI.rcMonitor.right - mI.rcMonitor.left,
							mI.rcMonitor.bottom - mI.rcMonitor.top,
							SWP_NOZORDER | SWP_FRAMECHANGED);
					}
				}
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else // if already full screen 
	{
		SetWindowLong(gHwnd, GWL_STYLE, dw_style | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

int initialize(void)
{
	// method declarations
	void resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	// initialize pfd structure

	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// warm up call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);

	return 0;
}

void uninitialize(void)
{
	if (gbFullScreen == TRUE) {
		toggleFullScreen();
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghrc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);

	glBegin(GL_LINES);

	float x1 = 0.0f, x2 = 1.0f, x3 = -1.0f;
	float y1 = 1.0f, y2 = -1.0f, y3 = -1.0f;

	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(x1, y1, 0.0f);
	glVertex3f(x3, y3, 0.0f);

	glVertex3f(x3, y3, 0.0f);
	glVertex3f(x2, y2, 0.0f);

	glVertex3f(x2, y2, 0.0f);
	glVertex3f(x1, y1, 0.0f);

	glEnd();

	float a = sqrtf(fabs((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2)));
	float b = sqrtf(fabs((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1)));
	float c = sqrtf(fabs((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)));

	float areaOfTriangle =
		(fabs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2))) / 2.0f;

	float perimeterOfCircle = a + b + c;
	//float semiPerimeter = perimeterOfCircle / 2.0f;

	float radius = (2 * areaOfTriangle) / perimeterOfCircle;

	float originX = (a * x1 + b * x2 + c * x3) / perimeterOfCircle;
	float originY = (a * y1 + b * y2 + c * y3) / perimeterOfCircle;

	glPointSize(2.5f);
	glBegin(GL_POINTS);

	for (float angle = 0.0f; angle < 2.0f * M_PI; angle += 0.001f)
	{
		glVertex3f(originX + (cos(angle) * radius),
			originY + (sin(angle) * radius), 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
	}

	glEnd();

	float xIntersection = (a * x1 + b * x2 + c * x3) / perimeterOfCircle;
	float yIntersection = (a * y1 + b * y2 + c * y3) / perimeterOfCircle;

	glLineWidth(2.0f);
	glBegin(GL_LINES);

	glVertex3f(x1, y1, 0.0f);
	glVertex3f(xIntersection, yIntersection, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);

	glVertex3f(xIntersection, yIntersection, 0.0f);
	glVertex3f((x3 - y3) / 2.0f, y3, 0.0f);
	glEnd();

	SwapBuffers(ghdc);
}