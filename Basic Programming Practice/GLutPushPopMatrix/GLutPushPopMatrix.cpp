#include <GL/freeglut.h>

bool bIsFullScreen = false;
int angle = 360;

int main(int argc, char **argv)
{
	// function declarations
	void initialize(void);
	void uninitialize(void);
	void reshape(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void update(void);

	// code
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("glut push-matrix and pop-matrix");

	initialize();

	// callbacks
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);
	glutMainLoop();

	return 0;
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glShadeModel(GL_FLAT);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void uninitialize(void)
{
	// code
}

void reshape(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

int year = 0;
int day = 0;

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0f, 0.0f, 5.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f);

	glPushMatrix();

	glutWireSphere(1.0f, 20.0f, 16.0f);
	glRotatef(year, 0.0f, 1.0f, 0.0f);
	glTranslatef(2.0f, 0.0f, 0.0f);
	glRotatef(day, 0.0f, 1.0f, 0.0f);
	glutWireSphere(0.2f, 10.0f, 8.0f);
	glPopMatrix();
	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key)
	{
	case 27: glutLeaveMainLoop();	// Esx key
		break;
	case 'f':
	case 'F':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	case 'Y': year = (year + 5) % angle;
		glutPostRedisplay();
		break;
	case 'y': year = (year - 5) % angle;
		glutPostRedisplay();
		break;
	case 'D': day = (day + 10) % angle;
		glutPostRedisplay();
		break;
	case 'd': day = (day - 10) % angle;
		glutPostRedisplay();
		break;
	}//switch
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON: break;
	case GLUT_RIGHT_BUTTON: glutLeaveMainLoop();
		break;
	}
}

