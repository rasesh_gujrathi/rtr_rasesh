#include <Windows.h>
#include <assert.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <stdio.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// Global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// open-gl rendering context
HWND gHwnd = NULL;
DWORD dw_style;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

typedef struct Light
{
	GLfloat ambient[4] = { 0.0f };
	GLfloat deffuse[4] = { 0.0f };
	GLfloat specular[4] = { 0.0f };
	GLfloat position[4] = { 0.0f };
} GLLight;

bool gbActiveWindow = false;
bool gbFullScreen = false;
bool bLight = false;
GLfloat lightAmbientZero[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuseZero[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightSpecularZero[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightPositionZero[4] = { -2.0f, 0.0f, 0.0f, 1.0f };

GLfloat lightAmbientOne[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuseOne[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat lightSpecularOne[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat lightPositionOne[4] = { 2.0f, 0.0f, 0.0f, 1.0f };

GLLight lights[2];

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat materialDiffuse[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat materialPosition[] = { 2.0f, 0.0f, 0.0f, 1.0f };
GLfloat materialShininess[] = { 50.0f };	

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void toggleFullScreen(void);
void resize(int width, int height);
void uninitialize(void);
void display(void);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// method declarations
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	int initialize(void);
	int iRet = 0;
	bool bDone = false;

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("Red and Blue Cross Lights on Black and White Pyramid..."),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS |
		WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	assert(hwnd);

	iRet = initialize();

	if (iRet < 0)
	{
		// error handling
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}

int width = 0, height = 0;

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void toggleFullScreen(void);

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		gHwnd = hwnd;
		break;
	case WM_SETFOCUS: gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: gbActiveWindow = false;
		break;
	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(width, height);
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 70:
		case 102: toggleFullScreen();
			break;
		case 27:
			DestroyWindow(hwnd);
			break;
		case 76:
		case 108:
			if (bLight == false)
			{
				bLight = true;
				glEnable(GL_LIGHTING);
			}
			else
			{
				bLight = false;
				glDisable(GL_LIGHTING);
			}
			break;
		}
		break;
	case WM_ERASEBKGND:
		return (0);
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void toggleFullScreen(void)
{
	MONITORINFO mI;

	if (gbFullScreen == false)
	{
		dw_style = GetWindowLong(gHwnd, GWL_STYLE);

		if (dw_style & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev))
			{
				HMONITOR hMonitor = MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY);
				if (hMonitor)
				{
					mI.cbSize = sizeof(MONITORINFO);
					if (GetMonitorInfo(hMonitor, &mI))
					{
						SetWindowLong(gHwnd, GWL_STYLE, dw_style & ~WS_OVERLAPPEDWINDOW);

						SetWindowPos(gHwnd,
							HWND_TOP,
							mI.rcMonitor.left,
							mI.rcMonitor.top,
							mI.rcMonitor.right - mI.rcMonitor.left,
							mI.rcMonitor.bottom - mI.rcMonitor.top,
							SWP_NOZORDER | SWP_FRAMECHANGED);
					}
				}
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else // if already full screen 
	{
		SetWindowLong(gHwnd, GWL_STYLE, dw_style | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

int initialize(void)
{	
	// method declarations
	void resize(int, int);
	void defineLights();

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	// initialize pfd structure

	defineLights();

	memset((void*)& pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	glLightfv(GL_LIGHT0, GL_AMBIENT, lights[0].ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lights[0].deffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lights[0].specular);
	glLightfv(GL_LIGHT0, GL_POSITION, lights[0].position);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1, GL_AMBIENT, lights[1].ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lights[1].deffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lights[1].specular);
	glLightfv(GL_LIGHT1, GL_POSITION, lights[1].position);
	glEnable(GL_LIGHT1);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// warm up call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void uninitialize(void)
{
	if (gbFullScreen == TRUE) {
		toggleFullScreen();
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghrc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

static float angle = 360.0f;

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -6.0f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f); // Rotate around the y axis.

	glBegin(GL_TRIANGLES);

	// front
	glNormal3f(0.0f, 0.447214f, 0.894427f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

	glBegin(GL_TRIANGLES);

	// right
	glNormal3f(0.894427f, 4472140.f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glEnd();

	glBegin(GL_TRIANGLES);

	// back
	glNormal3f(0.0f, 0.447214f, -0.894427f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glEnd();

	glBegin(GL_TRIANGLES);

	// left
	glNormal3f(-0.894427f, 0.447214f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glEnd();
	SwapBuffers(ghdc);
}

void update(void)
{
	angle -= 1;

	if (angle <= 0.0f)
		angle = 360.0f;
}

void defineLights()
{
	lights[0].ambient[0] = lightAmbientZero[0];
	lights[0].ambient[1] = lightAmbientZero[1];
	lights[0].ambient[2] = lightAmbientZero[2];
	lights[0].ambient[3] = lightAmbientZero[3];

	lights[0].deffuse[0] = lightDiffuseZero[0];
	lights[0].deffuse[1] = lightDiffuseZero[1];
	lights[0].deffuse[2] = lightDiffuseZero[2];
	lights[0].deffuse[3] = lightDiffuseZero[3];

	lights[0].specular[0] = lightSpecularZero[0];
	lights[0].specular[1] = lightSpecularZero[1];
	lights[0].specular[2] = lightSpecularZero[2];
	lights[0].specular[3] = lightSpecularZero[3];

	lights[0].position[0] = lightPositionZero[0];
	lights[0].position[1] = lightPositionZero[1];
	lights[0].position[2] = lightPositionZero[2];
	lights[0].position[3] = lightPositionZero[3];

	lights[1].ambient[0] = lightAmbientOne[0];
	lights[1].ambient[1] = lightAmbientOne[1];
	lights[1].ambient[2] = lightAmbientOne[2];
	lights[1].ambient[3] = lightAmbientOne[3];

	lights[1].deffuse[0] = lightDiffuseOne[0];
	lights[1].deffuse[1] = lightDiffuseOne[1];
	lights[1].deffuse[2] = lightDiffuseOne[2];
	lights[1].deffuse[3] = lightDiffuseOne[3];

	lights[1].specular[0] = lightSpecularOne[0];
	lights[1].specular[1] = lightSpecularOne[1];
	lights[1].specular[2] = lightSpecularOne[2];
	lights[1].specular[3] = lightSpecularOne[3];

	lights[1].position[0] = lightPositionOne[0];
	lights[1].position[1] = lightPositionOne[1];
	lights[1].position[2] = lightPositionOne[2];
	lights[1].position[3] = lightPositionOne[3];
}
