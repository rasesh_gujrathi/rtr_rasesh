#define _USE_MATH_DEFINES 1

#include <Windows.h>
#include <assert.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <math.h>
#include <stdio.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// Global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// open-gl rendering context
HWND gHwnd = NULL;
DWORD dw_style;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbFullScreen = false;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void toggleFullScreen(void);
void resize(int width, int height);
void uninitialize(void);
void display(void);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	int initialize(void);
	int iRet = 0;
	bool bDone = false;

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("STATIC INDIA"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS |
		WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	assert(hwnd);

	iRet = initialize();

	if (iRet < 0)
	{
		// error handling
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// call update
			}
			display();
		}
	}

	return((int)msg.wParam);
}

int width = 0, height = 0;

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void toggleFullScreen(void);

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		gHwnd = hwnd;
		break;
	case WM_SETFOCUS: gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: gbActiveWindow = false;
		break;
	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(width, height);
		break;
	case WM_KEYDOWN:
		toggleFullScreen();

		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_ERASEBKGND:
		return (0);
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void toggleFullScreen(void)
{
	MONITORINFO mI;

	if (gbFullScreen == false)
	{
		dw_style = GetWindowLong(gHwnd, GWL_STYLE);

		if (dw_style & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev))
			{
				HMONITOR hMonitor = MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY);
				if (hMonitor)
				{
					mI.cbSize = sizeof(MONITORINFO);
					if (GetMonitorInfo(hMonitor, &mI))
					{
						SetWindowLong(gHwnd, GWL_STYLE, dw_style &~WS_OVERLAPPEDWINDOW);

						SetWindowPos(gHwnd,
							HWND_TOP,
							mI.rcMonitor.left,
							mI.rcMonitor.top,
							mI.rcMonitor.right - mI.rcMonitor.left,
							mI.rcMonitor.bottom - mI.rcMonitor.top,
							SWP_NOZORDER | SWP_FRAMECHANGED);
					}
				}
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else // if already full screen 
	{
		SetWindowLong(gHwnd, GWL_STYLE, dw_style | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

int initialize(void)
{
	// method declarations
	void resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	// initialize pfd structure

	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// warm up call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	toggleFullScreen();

	return 0;
}

void uninitialize(void)
{
	if (gbFullScreen == TRUE) {
		toggleFullScreen();
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghrc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	/*glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);*/
}

void display(void)
{
	float upperLeftX = -0.8f;
	float upperLeftY = 0.7f;
	float widthX = 0.2f;
	float heightY = 0.1f;
	float kesariR = 1.0f;
	float kesariG = 0.5019607843137255f;
	float kesariB = 0.0f;
	float lineWidth = 0.07f;
	float greenR = 0.0f;
	float greenG = 0.5019607843137255f;
	float greenB = 0.0f;
	float wordDist = 0.05f;

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
	//glTranslatef(0.0f, 0.0f, -3.0f);

	// I
	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX, upperLeftY, 0.0f);
	glVertex3f(upperLeftX + widthX, upperLeftY, 0.0f);
	glVertex3f(upperLeftX + widthX, upperLeftY - heightY, 0.0f);
	glVertex3f(upperLeftX, upperLeftY - heightY, 0.0f);
	glEnd();

	glLoadIdentity();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + ((widthX - lineWidth) / 2.0f), upperLeftY - heightY, 0.0f);
	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + ((widthX + lineWidth) / 2.0f), upperLeftY - heightY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + ((widthX + lineWidth) / 2.0f), -(upperLeftY - heightY), 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + ((widthX - lineWidth) / 2.0f), -(upperLeftY - heightY), 0.0f);
	glEnd();

	glLoadIdentity();

	glBegin(GL_QUADS);

	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX, -(upperLeftY - heightY), 0.0f);
	glVertex3f(upperLeftX + widthX, -(upperLeftY - heightY), 0.0f);
	glVertex3f(upperLeftX + widthX, -(upperLeftY - heightY) - heightY, 0.0f);
	glVertex3f(upperLeftX, -(upperLeftY - heightY) - heightY, 0.0f);
	glEnd();

	// N
	glLoadIdentity();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX + wordDist, upperLeftY, 0.0f);
	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX + wordDist + lineWidth, upperLeftY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + widthX  + wordDist + lineWidth, -(upperLeftY - heightY)- heightY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + widthX  + wordDist, -(upperLeftY - heightY) - heightY, 0.0f);
	glEnd();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX + wordDist, upperLeftY, 0.0f);
	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX + wordDist + lineWidth, upperLeftY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist + lineWidth, -(upperLeftY - heightY) - heightY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist, -(upperLeftY - heightY) - heightY, 0.0f);
	glEnd();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist, upperLeftY, 0.0f);
	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist + lineWidth, upperLeftY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist + lineWidth, -(upperLeftY - heightY) - heightY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist, -(upperLeftY - heightY) - heightY, 0.0f);
	glEnd();

	// D
	glLoadIdentity();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist * 2 + lineWidth, upperLeftY, 0.0f);
	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist * 2 + lineWidth * 2, upperLeftY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist * 2 + lineWidth * 2, -(upperLeftY - heightY) - heightY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist * 2 + lineWidth, -(upperLeftY - heightY) - heightY, 0.0f);
	glEnd();
	
	glBegin(GL_POLYGON);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist * 2 + lineWidth * 2, upperLeftY, 0.0f);
	glVertex3f(upperLeftX + widthX * 3 + wordDist * 2 + lineWidth * 2.5f, upperLeftY, 0.0f);
	glVertex3f(upperLeftX + widthX * 3 + wordDist * 3 + lineWidth * 2.5f, upperLeftY - heightY + 0.04f, 0.0f);
	glVertex3f(upperLeftX + widthX * 3 + wordDist * 3 + lineWidth * 2.5f, upperLeftY - heightY, 0.0f);
	glVertex3f(upperLeftX + widthX * 2 + wordDist * 2 + lineWidth * 2, upperLeftY - heightY, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);

	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + widthX * 2 + wordDist * 2 + lineWidth * 2, -(upperLeftY - heightY), 0.0f);
	glVertex3f(upperLeftX + widthX * 3 + wordDist * 3 + lineWidth * 2.5f, -(upperLeftY - heightY), 0.0f);
	glVertex3f(upperLeftX + widthX * 3 + wordDist * 3 + lineWidth * 2.5f, -(upperLeftY - heightY) - 0.04f, 0.0f);
	glVertex3f(upperLeftX + widthX * 3 + wordDist * 2 + lineWidth * 2.5f, -(upperLeftY - heightY) - heightY, 0.0f);
	glVertex3f(upperLeftX + widthX * 2 + wordDist * 2 + lineWidth * 2, -(upperLeftY - heightY) - heightY, 0.0f);
	glEnd();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX * 3 + wordDist * 3 + lineWidth, upperLeftY - heightY, 0.0f);
	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX * 3 + wordDist * 3 + lineWidth * 2.5, upperLeftY - heightY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + widthX * 3 + wordDist * 3 + lineWidth * 2.5, -(upperLeftY - heightY) - heightY + 0.1f, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + widthX * 3 + wordDist * 3 + lineWidth, -(upperLeftY - heightY) - heightY + 0.1f, 0.0f);
	glEnd();

	// I
	upperLeftX += upperLeftX + (widthX * 6.8f) + (wordDist * 6.8f) + lineWidth;

	glLoadIdentity();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX, upperLeftY, 0.0f);
	glVertex3f(upperLeftX + widthX, upperLeftY, 0.0f);
	glVertex3f(upperLeftX + widthX, upperLeftY - heightY, 0.0f);
	glVertex3f(upperLeftX, upperLeftY - heightY, 0.0f);
	glEnd();

	glLoadIdentity();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + ((widthX - lineWidth) / 2.0f), upperLeftY - heightY, 0.0f);
	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + ((widthX + lineWidth) / 2.0f), upperLeftY - heightY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + ((widthX + lineWidth) / 2.0f), -(upperLeftY - heightY), 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + ((widthX - lineWidth) / 2.0f), -(upperLeftY - heightY), 0.0f);
	glEnd();

	glLoadIdentity();

	glBegin(GL_QUADS);

	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX, -(upperLeftY - heightY), 0.0f);
	glVertex3f(upperLeftX + widthX, -(upperLeftY - heightY), 0.0f);
	glVertex3f(upperLeftX + widthX, -(upperLeftY - heightY) - heightY, 0.0f);
	glVertex3f(upperLeftX, -(upperLeftY - heightY) - heightY, 0.0f);
	glEnd();

	// A

	upperLeftX += upperLeftX + (widthX * 0.4f) + (wordDist * 0.4f) + lineWidth;

	glLoadIdentity();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX, upperLeftY, 0.0f);
	glVertex3f(upperLeftX + widthX/1.5f, upperLeftY, 0.0f);
	glVertex3f(upperLeftX + widthX/1.5f, upperLeftY - heightY, 0.0f);
	glVertex3f(upperLeftX, upperLeftY - heightY, 0.0f);
	glEnd();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX, upperLeftY - heightY, 0.0f);
	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX/3.5f, upperLeftY - heightY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX - 0.04f, -(upperLeftY - heightY) - heightY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX - 0.10f , -(upperLeftY - heightY) - heightY, 0.0f);
	glEnd();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX / 3.5f + 0.02f, upperLeftY - heightY, 0.0f);
	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + widthX / 3.5f + 0.076f, upperLeftY - heightY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + 0.26f, -(upperLeftY - heightY) - heightY, 0.0f);
	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + 0.2f, -(upperLeftY - heightY) - heightY, 0.0f);
	glEnd();

	glBegin(GL_QUADS);

	glColor3f(kesariR, kesariG, kesariB);
	glVertex3f(upperLeftX + 0.027, upperLeftY - 0.50,  0.0f);
	glVertex3f(upperLeftX + 0.115 , upperLeftY - 0.50, 0.0f);
	glVertex3f(upperLeftX + 0.117f, upperLeftY - 0.52, 0.0f);
	glVertex3f(upperLeftX + 0.025, upperLeftY - 0.52, 0.0f);
	glEnd();

	glBegin(GL_QUADS);

	glColor3f(1, 1, 1);
	glVertex3f(upperLeftX + 0.025, upperLeftY - 0.52, 0.0f);
	glVertex3f(upperLeftX + 0.117, upperLeftY - 0.52, 0.0f);
	glVertex3f(upperLeftX + 0.119f, upperLeftY - 0.54, 0.0f);
	glVertex3f(upperLeftX + 0.023, upperLeftY - 0.54, 0.0f);
	glEnd();

	glBegin(GL_QUADS);

	glColor3f(greenR, greenG, greenB);
	glVertex3f(upperLeftX + 0.0235, upperLeftY - 0.54, 0.0f);
	glVertex3f(upperLeftX + 0.120, upperLeftY - 0.54, 0.0f);
	glVertex3f(upperLeftX + 0.122f, upperLeftY - 0.56, 0.0f);
	glVertex3f(upperLeftX + 0.022, upperLeftY - 0.56, 0.0f);
	glEnd();

	SwapBuffers(ghdc);
}