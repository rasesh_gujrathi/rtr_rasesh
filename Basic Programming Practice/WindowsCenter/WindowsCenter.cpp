#include <Windows.h>
#include <assert.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR szAppName[] = TEXT("Center window APP");
	HWND hwnd;

	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_HREDRAW | CS_VREDRAW; 

	RegisterClassEx(&wndclass);

	int x = 0, y = 0, m_hight = 800, m_width = 1000;

	MONITORINFO mi;
	HMONITOR hMonitor = MonitorFromWindow(NULL, MONITORINFOF_PRIMARY);
	mi.cbSize = sizeof(MONITORINFO);
	if (GetMonitorInfo(hMonitor, &mi))
	{
		x = (mi.rcMonitor.right / 2) - m_width / 2;
		y = (mi.rcMonitor.bottom / 2) - m_hight / 2;
	}

	hwnd = CreateWindow(szAppName, TEXT("Windows WM_CHAR"), WS_OVERLAPPEDWINDOW,
		x, y, m_width, m_hight, NULL, NULL,
		hInstance, NULL);

	assert(hwnd);
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_DESTROY: PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}