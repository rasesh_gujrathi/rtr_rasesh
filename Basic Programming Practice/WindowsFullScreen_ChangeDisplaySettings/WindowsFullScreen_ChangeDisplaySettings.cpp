#include <Windows.h>
#include <assert.h>

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Windows_WMCHAR");

	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_VREDRAW | CS_HREDRAW;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName, TEXT("Window Full Screen - ChangeDisplaySettings"), WS_OVERLAPPEDWINDOW,
		100, 200, 800, 600, NULL, NULL,
		hInstance, NULL);

	assert(hwnd);
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}

BOOL bFullScreen = false;
DWORD dw_style;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void toggleFullScreen(HWND);

	switch (iMsg)
	{
	case WM_CREATE:
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 70:
		case 102:
			toggleFullScreen(hwnd);
			break;
		}
		break;
	case WM_DESTROY: PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void toggleFullScreen(HWND hwnd)
{
	if (!bFullScreen)
	{
		dw_style = GetWindowLong(hwnd, GWL_STYLE);

		if (dw_style & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(hwnd, &wpPrev))
			{
				DEVMODE displaySettings;
				EnumDisplaySettings(NULL, 0, &displaySettings);
				ChangeDisplaySettings(&displaySettings, CDS_FULLSCREEN);
			}
		}

		ShowCursor(FALSE);
		bFullScreen = TRUE;
	}
	else
	{
		ChangeDisplaySettings(NULL, 0);
		ShowCursor(TRUE);
		bFullScreen = FALSE;
	}
}
