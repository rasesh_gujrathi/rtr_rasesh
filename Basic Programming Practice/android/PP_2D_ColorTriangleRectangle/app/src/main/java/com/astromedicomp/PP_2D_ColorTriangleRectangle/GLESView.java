package com.astromedicomp.PP_2D_ColorTriangleRectangle;

// additional packages
// AppCompatTextView class

import androidx.appcompat.widget.AppCompatTextView;
import android.content.Context; // for Context class
import android.view.Gravity;    // for Gravity class
import android.graphics.Color;  // Color class
import android.view.MotionEvent;    // for MotionEvent class
import android.view.GestureDetector; // for GuestureDetector class
import android.view.GestureDetector.OnGestureListener; // for OnGestureListener class
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener class

// nio = Non-blocking on native I/O
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import android.opengl.Matrix;   // for Matrix math

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;   // version 3.2
// 10th extension of basic features of open-gl ES
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.content.Context;

public class GLESView extends GLSurfaceView implements 
OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
    private GestureDetector gestureDetector;
    private final Context context;
    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;
    
    private int[] vaoTriangle = new int[1];
    private int[] vaoRectangle = new int[1];

    private int[] vboTriangle = new int[1];
    private int[] vboColorTriangle = new int[1];
    private int[] vboRectangle = new int[1];
    private int[] vboColorRectangle = new int[1];

    private int mvpUniform;
    // 4x4 matrix
    private float[] perspectiveProjectionMatrix = new float[16]; 

    private int angleTriangle = 0; // initializes triangle's angle to 0.0f
    private int angleRectangle = 0; // initializes rectangle's angle to 0.0f

    private float[] rotationMatrix = new float[16];

    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        // create handler which is going to handle guestures
        gestureDetector = new GestureDetector(context, this, null, false);
        // set handler which is goinf to handle guestures
        gestureDetector.setOnDoubleTapListener(this); 
    }

    // implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: " + version);
        String shadingVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: shading version: " + shadingVersion);

        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
        update();
    }

    private void initialize()
    {
        // vertex shader
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        // write vertex shader code
        final String vertexShaderSourceCode = String.format(
        "#version 320 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec4 vColor;"  +
        "uniform mat4 u_mvp_matrix;" +
		"out vec4 out_color;" +
        "void main(void)" +
        "{" +
		"out_color = vColor;" +
        "gl_Position = u_mvp_matrix * vPosition;" +
        "}");
     
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        // local variables
        int[] shaderCompileStatus = new int[1];
        int[] infoLogLen = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, shaderCompileStatus, 0);

        if(shaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            // vector shader compilation failed
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLen, 0);

            if(infoLogLen[0] != 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR: vertex shader compilation error " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }
        
        // define fragment shader object
	    fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

	    // fragment shader code
	    final String fragmentShaderSourceCode = 
		"#version 320 es" +
        "\n" +
        "precision highp float;" +
		"in vec4 out_color;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = out_color;" +
        "}";
        
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject);

        shaderCompileStatus[0] = 0;
        infoLogLen[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, shaderCompileStatus, 0);

        if(shaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            // vector shader compilation failed
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLen, 0);

            if(infoLogLen[0] != 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: fragment shader compilation error " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // shader program object
        shaderProgramObject = GLES32.glCreateProgram();
        // attach vertex shader
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        // attach fragment shader
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
        // Link
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, 
        "vPosition");

        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, 
        "vColor");

        // Link the shader program
        GLES32.glLinkProgram(shaderProgramObject);
    
        // error handling
        int[] iProgramLinkStatus = new int[1];
        infoLogLen[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            // compilation failed
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLen, 0);

            if (infoLogLen[0] != 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
                System.out.println("RTR: shader program link error " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        final float[] triangleVertices = new float[]
        {
            0.0f, 1.0f, 0.0f,
		    -1.0f, -1.0f, 0.0f,
		    1.0f, -1.0f, 0.0f
        };

        final float[] triangleVerticesColor = new float[]
        {
            1.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 1.0f
        };	

        // create vaoTriangle
        GLES32.glGenVertexArrays(1, vaoTriangle, 0);
        GLES32.glBindVertexArray(vaoTriangle[0]);
        GLES32.glGenBuffers(1, vboTriangle, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboTriangle[0]);

        // convert array to such an buffer that we can pass to 
        // glBufferData using native I/O (5 steps)
        // step-1    
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4);

        // step-2
        // arrange the buffer in native byteorder (little endian to big endian)
        byteBuffer.order(ByteOrder.nativeOrder());

        // step-3
        // create the float type buffer and convert our
        // byte type buffer into float type buffer
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        // step-4
        // now put your array into the cooked buffer 'positionBuffer'
        positionBuffer.put(triangleVertices);

        // step-5
        // set the array at 0th position of the buffer
        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length * 4, positionBuffer,
        GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, 
        GLES32.GL_FLOAT, false, 0, 0);

        // null is not mapped to zero in java
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // vboColorTriangle    
        GLES32.glGenBuffers(1, vboColorTriangle, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColorTriangle[0]);

        // convert array to such an buffer that we can pass to 
        // glBufferData using native I/O (5 steps)
        // step-1    
        ByteBuffer byteBufferC = ByteBuffer.allocateDirect(triangleVerticesColor.length * 4);

        // step-2
        // arrange the buffer in native byteorder (little endian to big endian)
        byteBufferC.order(ByteOrder.nativeOrder());

        // step-3
        // create the float type buffer and convert our
        // byte type buffer into float type buffer
        FloatBuffer positionBufferC = byteBufferC.asFloatBuffer();

        // step-4
        // now put your array into the cooked buffer 'positionBuffer'
        positionBufferC.put(triangleVerticesColor);

        // step-5
        // set the array at 0th position of the buffer
        positionBufferC.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVerticesColor.length * 4, positionBufferC,
        GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, 
        GLES32.GL_FLOAT, false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindVertexArray(0);

        final float[] rectangleVertices = new float[]
        {
            1.0f, 1.0f, 0.0f,
            -1.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f
        };

        final float[] rectangleVerticesColor = new float[]
        {
            0.0f, 1.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f
        };

         // create vaoRectangle
         GLES32.glGenVertexArrays(1, vaoRectangle, 0);
         GLES32.glBindVertexArray(vaoRectangle[0]);
         GLES32.glGenBuffers(1, vboRectangle, 0);
         GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboRectangle[0]);
 
         // convert array to such an buffer that we can pass to 
         // glBufferData using native I/O (5 steps)
         // step-1    
         ByteBuffer byteBufferR = ByteBuffer.allocateDirect(rectangleVertices.length * 4);
 
         // step-2
         // arrange the buffer in native byteorder (little endian to big endian)
         byteBufferR.order(ByteOrder.nativeOrder());
 
         // step-3
         // create the float type buffer and convert our
         // byte type buffer into float type buffer
         FloatBuffer positionBufferR = byteBufferR.asFloatBuffer();
 
         // step-4
         // now put your array into the cooked buffer 'positionBuffer'
         positionBufferR.put(rectangleVertices);
 
         // step-5
         // set the array at 0th position of the buffer
         positionBufferR.position(0);
 
         GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, rectangleVertices.length * 4, positionBufferR,
         GLES32.GL_STATIC_DRAW);
 
         GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, 
         GLES32.GL_FLOAT, false, 0, 0);
 
         // null is not mapped to zero in java
         GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
 
         GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
 
         // vboColorRectangle
        GLES32.glGenBuffers(1, vboColorRectangle, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColorRectangle[0]);

        // convert array to such an buffer that we can pass to 
        // glBufferData using native I/O (5 steps)
        // step-1    
        byteBufferC = ByteBuffer.allocateDirect(rectangleVerticesColor.length * 4);

        // step-2
        // arrange the buffer in native byteorder (little endian to big endian)
        byteBufferC.order(ByteOrder.nativeOrder());

        // step-3
        // create the float type buffer and convert our
        // byte type buffer into float type buffer
        positionBufferC = byteBufferC.asFloatBuffer();

        // step-4
        // now put your array into the cooked buffer 'positionBuffer'
        positionBufferC.put(rectangleVerticesColor);

        // step-5
        // set the array at 0th position of the buffer
        positionBufferC.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, rectangleVerticesColor.length * 4, positionBufferC,
        GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR, 3, 
        GLES32.GL_FLOAT, false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindVertexArray(0);

        // no warm up resize call required as window is already full screen
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }//initialize

    private void uninitialize()
    {
        int[] shaderCount = new int[1];
        int[] shaderNumber = new int[1];

        if(shaderProgramObject != 0)
        {
            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            if(shaderCount[0] != 0)
            {
                int[] shaders = new int[1];
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], 
                shaderCount, 0, shaders, 0);

                for(shaderNumber[0] = 0; shaderNumber[0] < shaderCount[0]; shaderNumber[0]++)
                {
                    // detach shader
                    GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber[0]]);  
                    // delete detach shader
                    GLES32.glDeleteShader(shaders[shaderNumber[0]]);   
                    shaders[shaderNumber[0]] = 0;       
                }//for
            }//if(shaderCount[0] != 0)

            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
            GLES32.glUseProgram(0);
        }//if(shaderProgramObject != 0)

        if (vaoTriangle[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vaoTriangle, 0);
            vaoTriangle[0] = 0;
        }
    
        if (vboTriangle[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboTriangle, 0);
            vboTriangle[0] = 0;
        }

        if (vboColorTriangle[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboColorTriangle, 0);
            vboColorTriangle[0] = 0;
        }

        if (vaoRectangle[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vaoRectangle, 0);
            vaoRectangle[0] = 0;
        }
    
        if (vboRectangle[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboRectangle, 0);
            vboRectangle[0] = 0;
        }

        if (vboColorRectangle[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboColorRectangle, 0);
            vboColorRectangle[0] = 0;
        }
    }

    private void resize(int width, int height)
    {
        if (height == 0)
		    height = 1;

        GLES32.glViewport(0, 0, width, height);

	    Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
	    GLES32.glUseProgram(shaderProgramObject);

        // declaration of matrices
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

	    // initialization of above matrix to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        // do necessary transformation
        Matrix.translateM(modelViewMatrix, 0, -1.0f, 0.0f, -3.0f);
        Matrix.rotateM(rotationMatrix, 0, angleTriangle, 1.0f, 0.0f, 0.0f);

        // do matrix multiplication
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, modelViewProjectionMatrix, 0, rotationMatrix, 0);

        // send necessary matrices to shaders in respective uniforms
	    // mvpUniform = glGetUniformLocation(...) 
	    // so mvpUniform on CPU and u_mvp_matrix is on GPU
	    // transpose means interchanging matrix row and column which is required in direct-x 
	    // but not in open-gl so GL_FALSE
	    GLES32.glUniformMatrix4fv(mvpUniform, 1 /* 1 array */, false, modelViewProjectionMatrix, 0);
        
        // rebind with vaoTriangle
	    GLES32.glBindVertexArray(vaoTriangle[0]);

	    // similarly bind with textures if any

	    // draw necessary scene
	    GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);		// GL_BEGIN

	    // unbind vaoTriangle
	    GLES32.glBindVertexArray(0);

        // rectangle

         // initialization of above matrix to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        // do necessary transformation
        Matrix.translateM(modelViewMatrix, 0, 1.0f, 0.0f, -3.0f);
        Matrix.rotateM(rotationMatrix, 0, angleRectangle, 0.0f, 1.0f, 0.0f);

        // do matrix multiplication
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, modelViewProjectionMatrix, 0, rotationMatrix, 0);

        // send necessary matrices to shaders in respective uniforms
	    // mvpUniform = glGetUniformLocation(...) 
	    // so mvpUniform on CPU and u_mvp_matrix is on GPU
	    // transpose means interchanging matrix row and column which is required in direct-x 
	    // but not in open-gl so GL_FALSE
	    GLES32.glUniformMatrix4fv(mvpUniform, 1 /* 1 array */, false, modelViewProjectionMatrix, 0);
        
        // rebind with vaoRectangle
	    GLES32.glBindVertexArray(vaoRectangle[0]);

	    // similarly bind with textures if any

	    // draw necessary scene
	    GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);		// GL_BEGIN

	    // unbind vaoRectangle
        GLES32.glBindVertexArray(0);
        
        GLES32.glUseProgram(0);
    
        requestRender();
    }

    private void update()
    {
        angleTriangle += 1;
    
        if (angleTriangle >= 360)
            angleTriangle = 0;
    
        angleRectangle += 1;
    
        if (angleRectangle >= 360)
            angleRectangle = 0;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return(true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onDown(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }

    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        
        System.exit(0);
        return(true);
    }

    @Override
    public void onShowPress(MotionEvent e)
    {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
}
