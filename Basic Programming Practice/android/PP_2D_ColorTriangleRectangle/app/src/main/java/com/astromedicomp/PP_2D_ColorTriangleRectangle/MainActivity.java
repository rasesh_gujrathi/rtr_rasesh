package com.astromedicomp.PP_2D_ColorTriangleRectangle;

// default packages
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
// additional packages
import android.view.Window;         // for Window class
import android.view.WindowManager;  // for WindowManager class
import android.content.pm.ActivityInfo; // for ActivityInfo class
import android.graphics.Color;  // Color class
import android.view.View;   // for View class

public class MainActivity extends AppCompatActivity {

    private GLESView glesview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_main);

        // remove the title bar
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        // make full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        // force landscape orientation
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // background color
        this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);

        // remove navigation
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        // Define our own view
        glesview = new GLESView(this);

        // set this view as out view
        setContentView(glesview);

        System.out.println("RTR: I am in onCreate. After setting features...");    
    }

    @Override 
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }
}

