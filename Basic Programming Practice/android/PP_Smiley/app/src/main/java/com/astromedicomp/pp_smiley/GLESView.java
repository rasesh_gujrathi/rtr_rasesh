// rasesh@debian:~/AndroidStudioProjects/09_PP_smiley$ 
// sudo ~/Android/Sdk/platform-tools/adb -d install -r app/build/outputs/apk/debug/app-debug.apk
package com.astromedicomp.pp_smiley;

// additional packages
// AppCompatTextView class

import androidx.appcompat.widget.AppCompatTextView;
import android.content.Context; // for Context class
import android.view.Gravity;    // for Gravity class
import android.graphics.Color;  // Color class
import android.view.MotionEvent;    // for MotionEvent class
import android.view.GestureDetector; // for GuestureDetector class
import android.view.GestureDetector.OnGestureListener; // for OnGestureListener class
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener class

import android.graphics.ColorMatrix;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

// nio = Non-blocking on native I/O
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import android.opengl.Matrix;   // for Matrix math

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;   // version 3.2
// 10th extension of basic features of open-gl ES
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.content.Context;

public class GLESView extends GLSurfaceView implements 
OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
    private GestureDetector gestureDetector;
    private final Context context;
    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;
    private int[] vao = new int[1];
    private int[] vbo = new int[1];
    private int mvpUniform;
    // 4x4 matrix
    private float[] perspectiveProjectionMatrix = new float[16]; 
    private float angleCube = 0; // initializes cube's angle to 0.0f

    private int[] texture_smiley = new int[1];
    private int[] vboTexture = new int[1];
    private int[] samplerUniform = new int[1];

    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        // create handler which is going to handle guestures
        gestureDetector = new GestureDetector(context, this, null, false);
        // set handler which is goinf to handle guestures
        gestureDetector.setOnDoubleTapListener(this); 
    }

    // implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR: " + version);
        String shadingVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR: shading version: " + shadingVersion);

        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    private void initialize()
    {
        // vertex shader
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        // write vertex shader code
        final String vertexShaderSourceCode = String.format(
        "#version 320 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec2 vTexCord;" +
        "uniform mat4 u_mvp_matrix;" +
        "out vec4 out_color;" +
        "out vec2 out_texcord;" +
        "void main(void)" +
        "{" +
        "out_texcord = vTexCord;" +
        "gl_Position = u_mvp_matrix * vPosition;" +
        "}");
     
        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        
	    // compile the vertex shader
        GLES32.glCompileShader(vertexShaderObject);

        // local variables
        int[] shaderCompileStatus = new int[1];
        int[] infoLogLen = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, 
        shaderCompileStatus, 0);

        if(shaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            // vector shader compilation failed
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLen, 0);

            if(infoLogLen[0] != 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR: vertex shader compilation error " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }
        
        // define fragment shader object
	    fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

	    // fragment shader code
	    final String fragmentShaderSourceCode = String.format(
		"#version 320 es" +
        "\n" +
        "precision highp float;" +
		"in vec2 out_texcord;" +
		"uniform sampler2D u_sampler;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = texture(u_sampler, out_texcord);" +
		"}");
        
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject);

        shaderCompileStatus[0] = 0;
        infoLogLen[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, shaderCompileStatus, 0);

        if(shaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            // vector shader compilation failed
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLen, 0);

            if(infoLogLen[0] != 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: fragment shader compilation error " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // shader program object
        shaderProgramObject = GLES32.glCreateProgram();
        // attach vertex shader
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        // attach fragment shader
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
        // Link
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");

        GLES32.glBindAttribLocation(shaderProgramObject, 
        GLESMacros.AMC_ATTRIBUTE_TEXCORD, "vTexCord");

        // Link the shader program
        GLES32.glLinkProgram(shaderProgramObject);
    
        // error handling
        int[] iProgramLinkStatus = new int[1];
        infoLogLen[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            // compilation failed
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLen, 0);

            if (infoLogLen[0] != 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
                System.out.println("RTR: shader program link error " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        samplerUniform[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_sampler");
    
        final float[] cubeVertices = new float[]
	    {
            -1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, -1.0f,

            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,

            -1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,

            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, 1.0f, -1.0f,

            1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, 1.0f, -1.0f,

            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f
	    };

        final float[] cubeTexCords = new float[]
        {
            0.0f, 0.0f, 
            1.0f, 0.0f, 
            1.0f, 1.0f, 
            0.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f
        };

        // create vao
        GLES32.glGenVertexArrays(1, vao, 0);
        GLES32.glBindVertexArray(vao[0]);
        GLES32.glGenBuffers(1, vbo, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[0]);

        // convert array to such an buffer that we can pass to 
        // glBufferData using native I/O (5 steps)
        // step-1    
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4);

        // step-2
        // arrange the buffer in native byteorder (little endian to big endian)
        byteBuffer.order(ByteOrder.nativeOrder());

        // step-3
        // create the float type buffer and convert our
        // byte type buffer into float type buffer
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        // step-4
        // now put your array into the cooked buffer 'positionBuffer'
        positionBuffer.put(cubeVertices);

        // step-5
        // set the array at 0th position of the buffer
        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, 
        GLES32.GL_FLOAT, false, 0, 0);

        // null is not mapped to zero in java
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vboTexture, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboTexture[0]);

        // convert array to such an buffer that we can pass to 
        // glBufferData using native I/O (5 steps)
        // step-1    
        ByteBuffer byteBufferT = ByteBuffer.allocateDirect(cubeTexCords.length * 4);

        // step-2
        // arrange the buffer in native byteorder (little endian to big endian)
        byteBufferT.order(ByteOrder.nativeOrder());

        // step-3
        // create the float type buffer and convert our
        // byte type buffer into float type buffer
        FloatBuffer positionBufferT = byteBufferT.asFloatBuffer();

        // step-4
        // now put your array into the cooked buffer 'positionBuffer'
        positionBufferT.put(cubeTexCords);

        // step-5
        // set the array at 0th position of the buffer
        positionBufferT.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeTexCords.length * 4, positionBufferT, GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCORD, 2, 
        GLES32.GL_FLOAT, false, 0, 0);

        // null is not mapped to zero in java
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCORD);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindVertexArray(0);    // unbound

        // no warm up resize call required as window is already full screen
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

        texture_smiley[0] = loadTexture(R.raw.smiley_512x512);
        GLES32.glEnable(GLES32.GL_TEXTURE_2D);
    }//initialize

    private int loadTexture(int imageFileResourceId)
    {
        int[] texture = new int[1];
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceId, options);

        // load texture in memory
        GLES32.glGenTextures(1, texture, 0);
        
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
        
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, 
                               GLES32.GL_TEXTURE_MAG_FILTER, 
                               GLES32.GL_LINEAR);

        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, 
                               GLES32.GL_TEXTURE_MIN_FILTER, 
                               GLES32.GL_LINEAR_MIPMAP_LINEAR);

        GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 4);

        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);

        GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);

        return texture[0];
    }

    private void uninitialize()
    {
        int[] shaderCount = new int[1];
        int[] shaderNumber = new int[1];

        if(shaderProgramObject != 0)
        {
            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            if(shaderCount[0] != 0)
            {
                int[] shaders = new int[1];
                GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], 
                shaderCount, 0, shaders, 0);

                for(shaderNumber[0] = 0; shaderNumber[0] < shaderCount[0]; shaderNumber[0]++)
                {
                    // detach shader
                    GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber[0]]);  
                    // delete detach shader
                    GLES32.glDeleteShader(shaders[shaderNumber[0]]);   
                    shaders[shaderNumber[0]] = 0;       
                }//for
            }//if(shaderCount[0] != 0)

            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
            GLES32.glUseProgram(0);
        }//if(shaderProgramObject != 0)

        if (vao[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao, 0);
            vao[0] = 0;
        }
    
        if (vbo[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo, 0);
            vbo[0] = 0;
        }

        if (vboTexture[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vboTexture, 0);
            vbo[0] = 0;
        }
    }

    private void resize(int width, int height)
    {
        if (height == 0)
		    height = 1;

        GLES32.glViewport(0, 0, width, height);

	    Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
	    GLES32.glUseProgram(shaderProgramObject);

        // declaration of matrices
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];
        float[] rotationMatrix = new float[16];

        GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
	    GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_smiley[0]);
        GLES32.glUniform1i(samplerUniform[0], 0);
    
        // rebind with vao_rectangle
        GLES32.glBindVertexArray(vao[0]);
    
	    // initialization of above matrix to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        // do necessary transformation
        Matrix.translateM(modelViewMatrix, 0, 0.5f, 0.0f, -6.0f);

        Matrix.rotateM(rotationMatrix, 0, 90.0f, 1.0f, 0.0f, 0.0f);
        Matrix.rotateM(rotationMatrix, 0, angleCube, 0.0f, 1.0f, 0.0f);

        // do matrix multiplication
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, 
        perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0, 
        modelViewProjectionMatrix, 0, rotationMatrix, 0);

        // send necessary matrices to shaders in respective uniforms
	    // mvpUniform = glGetUniformLocation(...) 
	    // so mvpUniform on CPU and u_mvp_matrix is on GPU
	    // transpose means interchanging matrix row and column which is required in direct-x 
	    // but not in open-gl so GL_FALSE
        GLES32.glUniformMatrix4fv(mvpUniform, 1 /* 1 array */, false, 
        modelViewProjectionMatrix, 0);
        
        // rebind with vao
	    GLES32.glBindVertexArray(vao[0]);

	    // similarly bind with textures if any

	    // draw necessary scene
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);		// GL_BEGIN
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);		// GL_BEGIN
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);		// GL_BEGIN
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);		// GL_BEGIN
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);		// GL_BEGIN
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);		// GL_BEGIN

	    // unbind vao
	    GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);
        update();
        requestRender();
    }

    private void update()
    {
        angleCube += 0.5f;

        if (angleCube >= 360.0f)
            angleCube = 0.0f;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return(true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onDown(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }

    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        
        System.exit(0);
        return(true);
    }

    @Override
    public void onShowPress(MotionEvent e)
    {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
}
