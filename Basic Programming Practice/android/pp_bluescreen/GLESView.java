package com.astromedicomp.pp_bluescreen;

// additional packages
// AppCompatTextView class

import androidx.appcompat.widget.AppCompatTextView;
import android.content.Context; // for Context class
import android.view.Gravity;    // for Gravity class
import android.graphics.Color;  // Color class
import android.view.MotionEvent;    // for MotionEvent class
import android.view.GestureDetector; // for GuestureDetector class
import android.view.GestureDetector.OnGestureListener; // for OnGestureListener class
import android.view.GestureDetector.OnDoubleTapListener; // for OnDoubleTapListener class

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;   // version 3.2
// 10th extension of basic features of open-gl ES
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.content.Context;

public class GLESView extends GLSurfaceView implements 
OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
    private GestureDetector gestureDetector;
    private final Context context;

    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        // create handler which is going to handle guestures
        gestureDetector = new GestureDetector(context, this, null, false);
        // set handler which is goinf to handle guestures
        gestureDetector.setOnDoubleTapListener(this); 
    }

    // implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    private void initialize()
    {
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    }

    private void resize(int width, int height)
    {
        GLES32.glViewport(0, 0, width, height);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | 
        GLES32.GL_DEPTH_BUFFER_BIT);

        requestRender();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return(true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onDown(MotionEvent e)
    {
        return(true);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }

    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        System.exit(0);
        return(true);
    }

    @Override
    public void onShowPress(MotionEvent e)
    {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }
}
