#include <Windows.h>
#include <assert.h>
#include <stdio.h>
#include <d3d11.h>

#pragma comment(lib, "d3d11.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// Global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// open-gl rendering context
HWND gHwnd = NULL;
DWORD dw_style;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbFullScreen = false;

float gClearColor[4]; // RGBA
IDXGISwapChain* gpIDXGISwapChain = NULL; // for swap buffer
ID3D11Device* gpID3D11Device = NULL; // device
ID3D11DeviceContext* gpID3D11DeviceContext = NULL; // direct-x context
ID3D11RenderTargetView* gpID3D11RenderTargetView = NULL;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void toggleFullScreen(void);
HRESULT resize(int width, int height);
void uninitialize(void);
void display(void);
HRESULT initialize(void);

FILE* gpFile = NULL;
char gszLogFileName[] = "Log.txt";

bool writeLog(const char* mode, const char* text);

bool writeLog(const char* mode, const char* text)
{
	if (fopen_s(&gpFile, gszLogFileName, mode) != 0)
	{
		if (strncmp("w", mode, 2) == 0)
		{
			MessageBox(NULL, TEXT("Failed to create log file...\nExiting..."),
				TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
			exit(0);
		}
		else
			if (strncmp("a+", mode, 3) == 0)
			{
				MessageBox(NULL, TEXT("Failed to open log file...\n"),
					TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
			}
	}
	else
	{
		if (gpFile)
		{
			fprintf_s(gpFile, text);
			fclose(gpFile);
		}
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	HRESULT hresult = S_OK;
	bool bDone = false;

	// create log file
	writeLog("w", "Log file is successfully opended...\n");

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("Direct-3D Blue Window!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS |
		WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	assert(hwnd);

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	hresult = initialize();

	if (FAILED(hresult))
	{
		writeLog("a+", "initialize failed...\nExiting...\n");

		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		writeLog("a+", "initialize succeeded...\n");
	}

	//message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{

			}
			display();
		}
	}

	return((int)msg.wParam);
}

int width = 0, height = 0;

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void toggleFullScreen(void);
	HRESULT hr = S_OK;

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		gHwnd = hwnd;
		break;
	case WM_SETFOCUS: gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: gbActiveWindow = false;
		break;
	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);

		if (gpID3D11DeviceContext)
		{
			hr = resize(width, height);

			if (FAILED(hr))
			{
				writeLog("a+", "resize failed...\nExiting...\n");
			}
			else
			{
				writeLog("a+", "resize succeeded...\n");
			}
		}
		width = LOWORD(lParam);
		height = HIWORD(lParam);

		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
			// F or f
		case 0x46:
			toggleFullScreen();
			break;
		}
		break;
	case WM_ERASEBKGND:
		return (0);
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void toggleFullScreen(void)
{
	MONITORINFO mI;

	if (gbFullScreen == false)
	{
		dw_style = GetWindowLong(gHwnd, GWL_STYLE);

		if (dw_style & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev))
			{
				HMONITOR hMonitor = MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY);
				if (hMonitor)
				{
					mI.cbSize = sizeof(MONITORINFO);
					if (GetMonitorInfo(hMonitor, &mI))
					{
						SetWindowLong(gHwnd, GWL_STYLE, dw_style & ~WS_OVERLAPPEDWINDOW);

						SetWindowPos(gHwnd,
							HWND_TOP,
							mI.rcMonitor.left,
							mI.rcMonitor.top,
							mI.rcMonitor.right - mI.rcMonitor.left,
							mI.rcMonitor.bottom - mI.rcMonitor.top,
							SWP_NOZORDER | SWP_FRAMECHANGED);
					}
				}
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else // if already full screen 
	{
		SetWindowLong(gHwnd, GWL_STYLE, dw_style | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

HRESULT initialize(void)
{
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // default, lowest
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void*)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = gHwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext);

		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
	{
		writeLog("a+", "D3D11CreateDeviceAndSwapChain failed...\nExiting...\n");
	}
	else
	{
		writeLog("a+", "D3D11CreateDeviceAndSwapChain succeeded...\n");
		writeLog("a+", "The chosen driver is of...\n");

		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			writeLog("a+", "Driver type: hardware\n");
		}
		else
			if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
			{
				writeLog("a+", "Driver type: WARP\n");
			}
			else
				if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
				{
					writeLog("a+", "Driver type: reference\n");
				}
				else
				{
					writeLog("a+", "Driver type: unknown\n");
				}

		writeLog("a+", "The supported highest feature level is...\n");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			writeLog("a+", "Feature level: 11.0\n");
		}
		else
			if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
			{
				writeLog("a+", "Feature level: 10.1\n");
			}
			else
				if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
				{
					writeLog("a+", "Feature level: 10.0\n");
				}
				else
				{
					writeLog("a+", "Feature level: Unknown\n");
				}
	}

	// d3d clear  color (blue)
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 1.0f;
	gClearColor[3] = 1.0f;

	hr = resize(WIN_WIDTH, WIN_HEIGHT);

	if (FAILED(hr))
	{
		writeLog("a+", "resize failed...\nExiting...\n");
	}
	else
	{
		writeLog("a+", "resize succeeded...\n");
	}

	return S_OK;
}

void uninitialize(void)
{
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gbFullScreen == TRUE) {
		toggleFullScreen();
	}

	if (gpFile)
	{
		writeLog("a+", "uninitialize() succeeded...\nLog file is successfully closed.\n");
	}
}

HRESULT resize(int width, int height)
{
	HRESULT hr;

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain buffers accordingly
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	// again get back buffer from swap chain
	ID3D11Texture2D* pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	if (gpID3D11Device)
	{
		// again get render target view from d3d11 device using above back buffer
		hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer,
			NULL, &gpID3D11RenderTargetView);

		if (FAILED(hr))
		{
			writeLog("a+", "ID3D11Device::CreateRenderTargetView failed...\n");
		}
		else
		{
			writeLog("a+", "ID3D11Device::CreateRenderTargetView succeeded...\n");
		}
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// set render target view as render target
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

	// set viewport
	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	return S_OK;
}

void display(void)
{
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

	gpIDXGISwapChain->Present(0, 0);
}

