#include <Windows.h>
#include <assert.h>
#include <stdio.h>
#include <GL/glew.h>	// added for programmable pipeline
#include <gl/GL.h>
#include "vmath.h"
#include "Sphere.h"

#define _USE_MATH_DEFINES 1
#include <math.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")	// added for programmable pipeline for glew dependency
#pragma comment(lib, "Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumVertices;
unsigned int gNumElements;

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCORD
};

BOOL bLight = TRUE;
BOOL bVertexShader = FALSE;
BOOL bFragmentShader = FALSE;

// Global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// open-gl rendering context
HWND gHwnd = NULL;
DWORD dw_style;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;
mat4 modelMatrix;
mat4 viewMatrix;

GLint gVertexShaderObject_pv;	// per vertex
GLint gFragmentShederObject_pv; // per vertex

GLint gVertexShaderObject_pf;	// per fragment
GLint gFragmentShederObject_pf; // per fragment

GLint gShaderProgramObject;

bool gbActiveWindow = false;
bool gbFullScreen = false;

GLuint modelUniformMatrix;
GLuint viewUniformMatrix;
GLuint projectionMatrixUniform;

// light uniforms
GLuint laUniform;
GLuint lsUniform;
GLuint ldUniform;

// material/surface uniforms
GLuint kaUniform;
GLuint ksUniform;
GLuint kdUniform;

float materialShininess;
GLuint lightPositionUniform;
GLuint lKeyPressedUniform;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void toggleFullScreen(void);
void resize(int width, int height);
void uninitialize(void);
void display(void);
void createVertexShaderPerVertex();
void createVertexShaderPerFragment();
void destroyShader();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void update();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	int initialize(void);
	int iRet = 0;
	bool bDone = false;

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("Programmable pipeline - 3d Sphere with ADS light per vertex & fragment toggle!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS |
		WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	assert(hwnd);

	iRet = initialize();

	if (iRet < 0)
	{
		// error handling
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
			}
			display();
		}
	}

	return((int)msg.wParam);
}

int width = 0, height = 0;

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void toggleFullScreen(void);

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		gHwnd = hwnd;
		break;
	case WM_SETFOCUS: gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: gbActiveWindow = false;
		break;
	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(width, height);
		break;
	case WM_CHAR:		
		switch (wParam)
		{
			// q & Q
		case 81:
		case 113:
			DestroyWindow(hwnd);
			break;
			// v & V
		case 86:
		case 118:
			bVertexShader = TRUE;
			createVertexShaderPerVertex();
			break;
			// f & F
		case 70:
		case 102:
			bFragmentShader = TRUE;
			createVertexShaderPerFragment();
			break;
			// esc
		case 27:
			toggleFullScreen();
			break;
			// l, L KEY
		case 76:
		case 108:
			if (bLight == false)
			{
				bLight = true;
			}
			else
			{
				bLight = false;
			}
			break;
		default:
			destroyShader();
			bVertexShader = FALSE;
			bFragmentShader = FALSE;
			break;
		}
		break;
	case WM_ERASEBKGND:
		return (0);
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void toggleFullScreen(void)
{
	MONITORINFO mI;

	if (gbFullScreen == false)
	{
		dw_style = GetWindowLong(gHwnd, GWL_STYLE);

		if (dw_style & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev))
			{
				HMONITOR hMonitor = MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY);
				if (hMonitor)
				{
					mI.cbSize = sizeof(MONITORINFO);
					if (GetMonitorInfo(hMonitor, &mI))
					{
						SetWindowLong(gHwnd, GWL_STYLE, dw_style & ~WS_OVERLAPPEDWINDOW);

						SetWindowPos(gHwnd,
							HWND_TOP,
							mI.rcMonitor.left,
							mI.rcMonitor.top,
							mI.rcMonitor.right - mI.rcMonitor.left,
							mI.rcMonitor.bottom - mI.rcMonitor.top,
							SWP_NOZORDER | SWP_FRAMECHANGED);
					}
				}
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else // if already full screen 
	{
		SetWindowLong(gHwnd, GWL_STYLE, dw_style | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

int initialize(void)
{
	GLenum result;

	// method declarations
	void resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	// initialize pfd structure

	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	result = glewInit();	// enable all graphic library extensions i.e. core profile (only programmable pipeline)

	if (result != GLEW_OK)
	{
		uninitialize();
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	// warm up call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void uninitialize(void)
{
	if (gbFullScreen == TRUE) {
		toggleFullScreen();
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	destroyShader();	
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	// do necessary transformation
	modelMatrix = translate(0.2f, 0.0f, -2.0f);

	// do matrix multiplication
	// TRS - transform, rotate, scale

	// send necessary matrices to shaders in respective uniforms
	// mvpUniform = glGetUniformLocation(...) 
	// so mvpUniform on CPU and u_mvp_matrix is on GPU
	// transpose means interchanging matrix row and column which is required in direct-x 
	// but not in open-gl so GL_FALSE
	glUniformMatrix4fv(modelUniformMatrix, 1 /* 1 array */, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniformMatrix, 1 /* 1 array */, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1 /* 1 array */, GL_FALSE, perspectiveProjectionMatrix);

	GLfloat lightPosition[4] = { 100.0f, 100.0f, 100.0f, 1.0f };
	GLfloat lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat lightAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	GLfloat lightDefuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat materialDefuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat materialAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	GLfloat materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat material_Shininess = 50.0f;		// try 128.0

	if (bFragmentShader || bVertexShader) // deaw only if v or f key pressed
	{
		if (bLight == TRUE)
		{
			glUniform1i(lKeyPressedUniform, 1);
			glUniform1f(materialShininess, material_Shininess);

			// light
			glUniform3fv(laUniform, 1, lightAmbient);
			glUniform3fv(ldUniform, 1, lightDefuse);
			glUniform3fv(lsUniform, 1, lightSpecular);

			// material
			glUniform3fv(kaUniform, 1, materialAmbient);
			glUniform3fv(kdUniform, 1, materialDefuse);
			glUniform3fv(ksUniform, 1, materialSpecular);

			glUniform4fv(lightPositionUniform, 1, lightPosition); // equivalent to glLightfv in ffp
		}
		else
		{
			glUniform1i(lKeyPressedUniform, 0);
		}

		// *** bind vao ***
		glBindVertexArray(gVao_sphere);

		// similarly bind with textures if any

		// draw necessary scene
		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	}

	// *** unbind vao ***
	glBindVertexArray(0);

	// unbind vao
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

void createVertexShaderPerVertex()
{
	// define vertex shader object
	gVertexShaderObject_pv = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar* vertexShaderSourceCode_pv = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec4 u_light_position;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shininess;" \
		"out vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_cordinates = u_viewMatrix * u_modelMatrix * vPosition;" \
		"vec3 transformedNormal = normalize(mat3(u_viewMatrix * u_modelMatrix) * vNormal);" \
		"vec3 lightDirection = normalize(vec3(u_light_position) - eye_cordinates.xyz);" \
		"float transformed_dot_lightDirection = max(dot(lightDirection, transformedNormal), 0.0f);" \
		"vec3 reflection_vector = reflect(-lightDirection, transformedNormal);" \
		"vec3 viewer_vector = normalize(vec3(-eye_cordinates.xyz));" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 defuse = u_ld * u_kd * transformed_dot_lightDirection;" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, viewer_vector), 0.0f), u_material_shininess);" \
		"fong_ads_light = vec3(ambient + defuse + specular);" \
		"}" \
		"else" \
		"{" \
		"fong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject_pv, 1, (const GLchar**)&vertexShaderSourceCode_pv, NULL);

	// compile the vertex shader
	glCompileShader(gVertexShaderObject_pv);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLen = 0;
	char* szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject_pv, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gVertexShaderObject_pv, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_pv, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// define fragment shader object
	gFragmentShederObject_pv = glCreateShader(GL_FRAGMENT_SHADER);

	// fragment shader code
	const GLchar* fragmentShaderSourceCode_pv = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"in vec3 fong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(fong_ads_light, 1.0);" \
		"}";

	glShaderSource(gFragmentShederObject_pv, 1, (const GLchar**)&fragmentShaderSourceCode_pv, NULL);

	// compile the vertex shader
	glCompileShader(gFragmentShederObject_pv);

	iShaderCompileStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetShaderiv(gFragmentShederObject_pv, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gFragmentShederObject_pv, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShederObject_pv, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Create shader program object
	gShaderProgramObject = glCreateProgram();

	// Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject_pv);

	// Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShederObject_pv);

	// prelinking - binding to vertex attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link the shader program
	glLinkProgram(gShaderProgramObject);

	// error handling
	GLint iProgramLinkStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		// compilation failed
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	modelUniformMatrix = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininess = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	viewUniformMatrix = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);
}

void createVertexShaderPerFragment()
{
	// define vertex shader object
	gVertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar* vertexShaderSourceCode_pf = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform vec4 u_light_position;" \
		"out vec3 computeTransformedNormal;" \
		"out vec3 lightDirection;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_cordinates = u_viewMatrix * u_modelMatrix * vPosition;" \
		"computeTransformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal;" \
		"lightDirection = vec3(u_light_position) - eye_cordinates.xyz;" \
		"viewer_vector = vec3(-eye_cordinates.xyz);" \
		"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject_pf, 1, (const GLchar**)&vertexShaderSourceCode_pf, NULL);

	// compile the vertex shader
	glCompileShader(gVertexShaderObject_pf);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLen = 0;
	char* szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject_pf, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gVertexShaderObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_pf, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// define fragment shader object
	gFragmentShederObject_pf = glCreateShader(GL_FRAGMENT_SHADER);

	// fragment shader code
	// light calculations are done in fragment shader
	const GLchar* fragmentShaderSourceCode_pf = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"in vec3 computeTransformedNormal;" \
		"in vec3 lightDirection;" \
		"in vec3 viewer_vector;" \
		"vec3 fong_ads_light;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform float u_material_shininess;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec3 normalizedTransformedNormal = normalize(computeTransformedNormal);" \
		"vec3 normalizedLightDirection = normalize(lightDirection);" \
		"float transformed_dot_lightDirection = max(dot(normalizedLightDirection, normalizedTransformedNormal), 0.0f);" \
		"vec3 reflection_vector = reflect(-normalizedLightDirection, normalizedTransformedNormal);" \
		"vec3 normalized_viewer_vector = normalize(viewer_vector);" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 defuse = u_ld * u_kd * transformed_dot_lightDirection;" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0f), u_material_shininess);" \
		"fong_ads_light = vec3(ambient + defuse + specular);" \
		"FragColor = vec4(fong_ads_light, 1.0);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}" \
		"}";

	glShaderSource(gFragmentShederObject_pf, 1, (const GLchar**)&fragmentShaderSourceCode_pf, NULL);

	// compile the vertex shader
	glCompileShader(gFragmentShederObject_pf);

	iShaderCompileStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetShaderiv(gFragmentShederObject_pf, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gFragmentShederObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShederObject_pf, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Create shader program object
	gShaderProgramObject = glCreateProgram();

	// Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject_pf);

	// Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShederObject_pf);

	// prelinking - binding to vertex attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link the shader program
	glLinkProgram(gShaderProgramObject);

	// error handling
	GLint iProgramLinkStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		// compilation failed
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	modelUniformMatrix = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininess = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	viewUniformMatrix = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);
}

void destroyShader()
{
	if (bVertexShader)
	{
		glUseProgram(gShaderProgramObject);
		glDetachShader(gShaderProgramObject, gFragmentShederObject_pv);
		glDetachShader(gShaderProgramObject, gVertexShaderObject_pv);
		glDeleteShader(gFragmentShederObject_pv);
		gFragmentShederObject_pv = 0;
		glDeleteShader(gVertexShaderObject_pv);
		gVertexShaderObject_pv = 0;
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
	}

	if (bFragmentShader)
	{
		glUseProgram(gShaderProgramObject);
		glDetachShader(gShaderProgramObject, gFragmentShederObject_pf);
		glDetachShader(gShaderProgramObject, gVertexShaderObject_pf);
		glDeleteShader(gFragmentShederObject_pf);
		gFragmentShederObject_pf = 0;
		glDeleteShader(gVertexShaderObject_pf);
		gVertexShaderObject_pf = 0;
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
	}
	
	glUseProgram(0);
}