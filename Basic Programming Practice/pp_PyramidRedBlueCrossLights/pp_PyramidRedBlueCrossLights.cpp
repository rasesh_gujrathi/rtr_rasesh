#include <Windows.h>
#include <assert.h>
#include <stdio.h>
#include <GL/glew.h>	// added for programmable pipeline
#include <gl/GL.h>
#include "vmath.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")	// added for programmable pipeline for glew dependency

#define WIN_WIDTH 1024
#define WIN_HEIGHT 768

using namespace vmath;

static float anglePyramid = 0; // initializes pyramids's angle to 0.0f

mat4 rotationMatrix = mat4::identity();

typedef struct Light
{
	GLfloat ambient[4] = { 0.0f };
	GLfloat deffuse[4] = { 0.0f };
	GLfloat specular[4] = { 0.0f };
	GLfloat position[4] = { 0.0f };
} GLLight;

GLfloat material_shininess =  50.0f; // try 128

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCORD
};

// Global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// open-gl rendering context
HWND gHwnd = NULL;
DWORD dw_style;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

GLuint vao_triangle;
GLuint vao_rectangle;

// for position
GLuint vbo_triangle;

// for normals
GLuint vbo_normals;

GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;
mat4 modelMatrix;
mat4 viewMatrix;

GLint gVertexShaderObject;
GLint gFragmentShederObject;
GLint gShaderProgramObject;

bool gbActiveWindow = false;
bool gbFullScreen = false;

GLuint modelUniformMatrix;
GLuint viewUniformMatrix;
GLuint projectionMatrixUniform;

GLLight lights[2];

const GLfloat materialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat materialAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
const GLfloat materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

// light uniforms
GLuint laUniformRed;
GLuint lsUniformRed;
GLuint ldUniformRed;

GLuint laUniformBlue;
GLuint lsUniformBlue;
GLuint ldUniformBlue;

// material/surface uniforms
GLuint kaUniform;
GLuint ksUniform;
GLuint kdUniform;

float materialShininess;
GLuint lightPositionUniformRed;
GLuint lightPositionUniformBlue;
GLuint lKeyPressedUniform;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void toggleFullScreen(void);
void resize(int width, int height);
void uninitialize(void);
void display(void);
void defineLights();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void update();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	int initialize(void);
	int iRet = 0;
	bool bDone = false;

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("Programmable pipeline - Rotating pyramid - 2 lights!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS |
		WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	assert(hwnd);

	iRet = initialize();

	if (iRet < 0)
	{
		// error handling
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}

int width = 0, height = 0;

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void toggleFullScreen(void);

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		gHwnd = hwnd;
		break;
	case WM_SETFOCUS: gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: gbActiveWindow = false;
		break;
	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(width, height);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			toggleFullScreen();
			break;
		}
		break;
	case WM_ERASEBKGND:
		return (0);
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void toggleFullScreen(void)
{
	MONITORINFO mI;

	if (gbFullScreen == false)
	{
		dw_style = GetWindowLong(gHwnd, GWL_STYLE);

		if (dw_style & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev))
			{
				HMONITOR hMonitor = MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY);
				if (hMonitor)
				{
					mI.cbSize = sizeof(MONITORINFO);
					if (GetMonitorInfo(hMonitor, &mI))
					{
						SetWindowLong(gHwnd, GWL_STYLE, dw_style & ~WS_OVERLAPPEDWINDOW);

						SetWindowPos(gHwnd,
							HWND_TOP,
							mI.rcMonitor.left,
							mI.rcMonitor.top,
							mI.rcMonitor.right - mI.rcMonitor.left,
							mI.rcMonitor.bottom - mI.rcMonitor.top,
							SWP_NOZORDER | SWP_FRAMECHANGED);
					}
				}
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else // if already full screen 
	{
		SetWindowLong(gHwnd, GWL_STYLE, dw_style | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

int initialize(void)
{
	GLenum result;

	// method declarations
	void resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	defineLights();

	// code
	// initialize pfd structure

	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	result = glewInit();	// enable all graphic library extensions i.e. core profile (only programmable pipeline)

	if (result != GLEW_OK)
	{
		uninitialize();
	}

	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar* vertexShaderSourceCode = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec4 u_light_position_red;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shininess;" \
		"out vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"vec4 eye_cordinates = u_viewMatrix * u_modelMatrix * vPosition;" \
		"vec3 transformedNormal = normalize(mat3(u_viewMatrix * u_modelMatrix) * vNormal);" \
		"vec3 lightDirection_red = normalize(vec3(u_light_position_red) - eye_cordinates.xyz);" \
		"float transformed_dot_lightDirection_red = max(dot(lightDirection_red, transformedNormal), 0.0f);" \
		"vec3 reflection_vector_red = reflect(-lightDirection_red, transformedNormal);" \
		"vec3 viewer_vector = normalize(vec3(-eye_cordinates.xyz));" \
		"vec3 ambient_red = u_la_red * u_ka;" \
		"vec3 defuse_red = u_ld_red * u_kd * transformed_dot_lightDirection_red;" \
		"vec3 specular_red = u_ls_red * u_ks * pow(max(dot(reflection_vector_red, viewer_vector), 0.0f), u_material_shininess);" \

		"vec3 lightDirection_blue = normalize(vec3(u_light_position_blue) - eye_cordinates.xyz);" \
		"float transformed_dot_lightDirection_blue = max(dot(lightDirection_blue, transformedNormal), 0.0f);" \
		"vec3 reflection_vector_blue = reflect(-lightDirection_blue, transformedNormal);" \
		"vec3 ambient_blue = u_la_blue * u_ka;" \
		"vec3 defuse_blue = u_ld_blue * u_kd * transformed_dot_lightDirection_blue;" \
		"vec3 specular_blue = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue, viewer_vector), 0.0f), u_material_shininess);" \

		"fong_ads_light = vec3(ambient_red + defuse_red + specular_red + ambient_blue + defuse_blue + specular_blue);" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLen = 0;
	char* szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// define fragment shader object
	gFragmentShederObject = glCreateShader(GL_FRAGMENT_SHADER);

	// fragment shader code
	const GLchar* fragmentShaderSourceCode = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"in vec3 fong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(fong_ads_light, 1.0);" \
		"}";

	glShaderSource(gFragmentShederObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gFragmentShederObject);

	iShaderCompileStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetShaderiv(gFragmentShederObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gFragmentShederObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShederObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Create shader program object
	gShaderProgramObject = glCreateProgram();

	// Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShederObject);

	// prelinking - binding to vertex attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link the shader program
	glLinkProgram(gShaderProgramObject);

	// error handling
	GLint iProgramLinkStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		// compilation failed
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	modelUniformMatrix = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	laUniformRed = glGetUniformLocation(gShaderProgramObject, "u_la_red");
	laUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_la_blue");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	ldUniformRed = glGetUniformLocation(gShaderProgramObject, "u_ld_red");
	ldUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_ld_blue");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	lsUniformRed = glGetUniformLocation(gShaderProgramObject, "u_ls_red");
	lsUniformBlue= glGetUniformLocation(gShaderProgramObject, "u_ls_blue");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininess = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	viewUniformMatrix = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	lightPositionUniformRed = glGetUniformLocation(gShaderProgramObject, "u_light_position_red");
	lightPositionUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_light_position_blue");

	const GLfloat pyramidVertices[] =
	{
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f
	};

	// create vao for triangle model
	glGenVertexArrays(1, &vao_triangle);
	glBindVertexArray(vao_triangle);

	glGenBuffers(1, &vbo_triangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	const GLfloat sphere_normals[] =
	{
		0.894427f, 4472140.f, 0.0f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, -0.894427f,
		-0.894427f, 0.447214f, 0.0f
	};

	// normal vbo
	glGenBuffers(1, &vbo_normals);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// glEnable(GL_CULL_FACE); // disables model's backside rendering 
	// glDisable(GL_CULL_FACE); // default : enables model's backside rendering 

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	// warm up call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void uninitialize(void)
{
	if (gbFullScreen == TRUE) {
		toggleFullScreen();
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}

	if (vao_triangle)
	{
		glDeleteVertexArrays(1, &vao_triangle);
		vao_triangle = 0;
	}

	if (vao_rectangle)
	{
		glDeleteVertexArrays(1, &vao_rectangle);
		vao_rectangle = 0;
	}

	if (vbo_triangle)
	{
		glDeleteBuffers(1, &vbo_triangle);
		vbo_triangle = 0;
	}

	if (vbo_normals)
	{
		glDeleteBuffers(1, &vbo_normals);
		vbo_normals = 0;
	}

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShederObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShederObject);
	gFragmentShederObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	// declaration of matrices
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	// initialization of above matrix to identity
	modelMatrix = translate(0.5f, 0.0f, -6.0f);

	rotationMatrix = rotate(anglePyramid, 0.0f, 1.0f, 0.0f);

	// do necessary transformation

	// do matrix multiplication
	// TRS - transform, rotate, scale
	modelMatrix = modelMatrix * rotationMatrix;	// 1st Trs
	
	// send necessary matrices to shaders in respective uniforms
	// mvpUniform = glGetUniformLocation(...) 
	// so mvpUniform on CPU and u_mvp_matrix is on GPU
	// transpose means interchanging matrix row and column which is required in direct-x 
	// but not in open-gl so GL_FALSE
	glUniformMatrix4fv(modelUniformMatrix, 1 /* 1 array */, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniformMatrix, 1 /* 1 array */, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1 /* 1 array */, GL_FALSE, perspectiveProjectionMatrix);

	glUniform1f(materialShininess, material_shininess);

	// red light
	glUniform3fv(laUniformRed, 1, lights[0].ambient);
	glUniform3fv(ldUniformRed, 1, lights[0].deffuse);
	glUniform3fv(lsUniformRed, 1, lights[0].specular);
	glUniform4fv(lightPositionUniformRed, 1, lights[0].position); // equivalent to glLightfv in ffp

	// blue light
	glUniform3fv(laUniformBlue, 1, lights[1].ambient);
	glUniform3fv(ldUniformBlue, 1, lights[1].deffuse);
	glUniform3fv(lsUniformBlue, 1, lights[1].specular);
	glUniform4fv(lightPositionUniformBlue, 1, lights[1].position); // equivalent to glLightfv in ffp

	// material
	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);

	// rebind with vao_triangle
	glBindVertexArray(vao_triangle);

	// similarly bind with textures if any

	// draw necessary scene
	glDrawArrays(GL_TRIANGLES, 0, 12);		// GL_BEGIN

	// unbind vao_triangle
	glBindVertexArray(0);

	// rebind with vao_rectangle
	glBindVertexArray(vao_rectangle);

	// unbind vao_rectangle
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

void update(void)
{
	anglePyramid += 1;

	if (anglePyramid >= 360.0f)
		anglePyramid = 0.0f;
}

void defineLights()
{
	const GLfloat lightAmbientZero[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
	const GLfloat lightDiffuseZero[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
	const GLfloat lightSpecularZero[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
	const GLfloat lightPositionZero[4] = { -2.0f, 0.0f, 0.0f, 1.0f };

	const GLfloat lightAmbientOne[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
	const GLfloat lightDiffuseOne[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
	const GLfloat lightSpecularOne[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
	const GLfloat lightPositionOne[4] = { 2.0f, 0.0f, 0.0f, 1.0f };	

	lights[0].ambient[0] = lightAmbientZero[0];
	lights[0].ambient[1] = lightAmbientZero[1];
	lights[0].ambient[2] = lightAmbientZero[2];
	lights[0].ambient[3] = lightAmbientZero[3];

	lights[0].deffuse[0] = lightDiffuseZero[0];
	lights[0].deffuse[1] = lightDiffuseZero[1];
	lights[0].deffuse[2] = lightDiffuseZero[2];
	lights[0].deffuse[3] = lightDiffuseZero[3];

	lights[0].specular[0] = lightSpecularZero[0];
	lights[0].specular[1] = lightSpecularZero[1];
	lights[0].specular[2] = lightSpecularZero[2];
	lights[0].specular[3] = lightSpecularZero[3];

	lights[0].position[0] = lightPositionZero[0];
	lights[0].position[1] = lightPositionZero[1];
	lights[0].position[2] = lightPositionZero[2];
	lights[0].position[3] = lightPositionZero[3];

	lights[1].ambient[0] = lightAmbientOne[0];
	lights[1].ambient[1] = lightAmbientOne[1];
	lights[1].ambient[2] = lightAmbientOne[2];
	lights[1].ambient[3] = lightAmbientOne[3];

	lights[1].deffuse[0] = lightDiffuseOne[0];
	lights[1].deffuse[1] = lightDiffuseOne[1];
	lights[1].deffuse[2] = lightDiffuseOne[2];
	lights[1].deffuse[3] = lightDiffuseOne[3];

	lights[1].specular[0] = lightSpecularOne[0];
	lights[1].specular[1] = lightSpecularOne[1];
	lights[1].specular[2] = lightSpecularOne[2];
	lights[1].specular[3] = lightSpecularOne[3];

	lights[1].position[0] = lightPositionOne[0];
	lights[1].position[1] = lightPositionOne[1];
	lights[1].position[2] = lightPositionOne[2];
	lights[1].position[3] = lightPositionOne[3];
}
