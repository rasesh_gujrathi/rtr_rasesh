#include <Windows.h>
#include <stdio.h>
#include <GL/glew.h>	// added for programmable pipeline
#include <gl/GL.h>
#include "vmath.h"
#include "cuda_runtime.h"
#include "cuda_gl_interop.h"
#include "pp_cuda_sinewave.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")	// added for programmable pipeline for glew dependency
#pragma comment(lib, "cudart.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//const int glMeshWidth = 1024;
//const int glMeshHeight = 1024;

int glMeshWidth = 64;
int glMeshHeight = 64;

#define MYARRAYSIZE glMeshWidth * glMeshHeight * 4

//float pos[glMeshWidth][glMeshHeight][4];
float* pos = NULL;

struct cudaGraphicsResource* graphicsResource = NULL;
bool bOnGPU = false;

using namespace vmath;

void launchCPUKernel(unsigned int meshWidth, unsigned int meshHeight, float animationTime);

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCORD
};

// Global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// open-gl rendering context
HWND gHwnd = NULL;
DWORD dw_style;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

GLuint vao;
GLuint vbo_cpu;
GLuint vbo_gpu;

float animationTime = 0.0f;

cudaError_t error;
GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;

GLint gVertexShaderObject;
GLint gFragmentShederObject;
GLint gShaderProgramObject;

bool gbActiveWindow = false;
bool gbFullScreen = false;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void toggleFullScreen(void);
void resize(int width, int height);
void uninitialize(void);
void display(void);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	int initialize(void);
	int iRet = 0;
	bool bDone = false;

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("Programmable pipeline - Cuda Sinewave!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS |
		WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	iRet = initialize();

	if (iRet < 0)
	{
		// error handling
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{

			}
			display();
		}
	}

	return((int)msg.wParam);
}
int width, height = 0;

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void toggleFullScreen(void);

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		gHwnd = hwnd;
		break;
	case WM_SETFOCUS: gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: gbActiveWindow = false;
		break;
	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(width, height);
		break;
	case WM_KEYDOWN:
		
		switch (wParam)
		{
			// 1
		case 0x31:
			if (pos)
			{
				delete(pos);
				pos = NULL;
			}

			glMeshWidth = 64;
			glMeshHeight = 64;
			break;
			// 2
		case 0x32:
			if (pos)
			{
				delete(pos);
				pos = NULL;
			}

			glMeshWidth = 128;
			glMeshHeight = 128;
			break;
			 // 3
		case 0x33:
			if (pos)
			{
				delete(pos);
				pos = NULL;
			}

			glMeshWidth = 256;
			glMeshHeight = 256;
			break;
			// 4 
		case 0x34:
			if (pos)
			{
				delete(pos);
				pos = NULL;
			}

			glMeshWidth = 512;
			glMeshHeight = 512;
			break;
			// 5
		case 0x35:
			if (pos)
			{
				delete(pos);
				pos = NULL;
			}

			glMeshWidth = 1024;
			glMeshHeight = 1024;
			break;
			// 6
		case 0x36:
			if (pos)
			{
				delete(pos);
				pos = NULL;
			}

			glMeshWidth = 2048;
			glMeshHeight = 2048;
			break;
			// 7
		case 0x37:
			if (pos)
			{
				delete(pos);
				pos = NULL;
			}

			glMeshWidth = 4096;
			glMeshHeight = 4096;
			break;
			// 8
		case 0x38:
			if (pos)
			{
				delete(pos);
				pos = NULL;
			}

			glMeshWidth = 8192;
			glMeshHeight = 8192;
			break;
			// c & C - on CPU
		case 0x10:
		case 0x43:
			bOnGPU = false;
			break;
			// g & G - on GPU
		case 0x14:
		case 0x47:
			bOnGPU = true;
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			toggleFullScreen();
			break;
		}
		break;
	case WM_ERASEBKGND:
		return (0);
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void toggleFullScreen(void)
{
	MONITORINFO mI;

	if (gbFullScreen == false)
	{
		dw_style = GetWindowLong(gHwnd, GWL_STYLE);

		if (dw_style & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev))
			{
				HMONITOR hMonitor = MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY);
				if (hMonitor)
				{
					mI.cbSize = sizeof(MONITORINFO);
					if (GetMonitorInfo(hMonitor, &mI))
					{
						SetWindowLong(gHwnd, GWL_STYLE, dw_style & ~WS_OVERLAPPEDWINDOW);

						SetWindowPos(gHwnd,
							HWND_TOP,
							mI.rcMonitor.left,
							mI.rcMonitor.top,
							mI.rcMonitor.right - mI.rcMonitor.left,
							mI.rcMonitor.bottom - mI.rcMonitor.top,
							SWP_NOZORDER | SWP_FRAMECHANGED);
					}
				}
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else // if already full screen 
	{
		SetWindowLong(gHwnd, GWL_STYLE, dw_style | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

int initialize(void)
{
	GLenum result;
	int devCount = 0;

	// method declarations
	void resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	// initialize pfd structure

	error = cudaGetDeviceCount(&devCount);

	if (error != cudaSuccess)
	{
		return -1;
	}
	else
		if (devCount == 0)
		{
			return -1;
		}

	cudaSetDevice(0);

	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	result = glewInit();	// enable all graphic library extensions i.e. core profile (only programmable pipeline)

	if (result != GLEW_OK)
	{
		uninitialize();
	}

	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar* vertexShaderSourceCode = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLen = 0;
	char* szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// define fragment shader object
	gFragmentShederObject = glCreateShader(GL_FRAGMENT_SHADER);

	// fragment shader code
	const GLchar* fragmentShaderSourceCode = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}";

	glShaderSource(gFragmentShederObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gFragmentShederObject);

	iShaderCompileStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetShaderiv(gFragmentShederObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gFragmentShederObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShederObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Create shader program object
	gShaderProgramObject = glCreateProgram();

	// Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShederObject);

	// prelinking - binding to vertex attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	// Link the shader program
	glLinkProgram(gShaderProgramObject);

	// error handling
	GLint iProgramLinkStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		// compilation failed
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");	

	// create vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo_cpu);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cpu);
	glBufferData(GL_ARRAY_BUFFER, MYARRAYSIZE * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	/*glGenBuffers(1, &vbo_gpu);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_gpu);
	glBufferData(GL_ARRAY_BUFFER, MYARRAYSIZE * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	error = cudaGraphicsGLRegisterBuffer(&graphicsResource, vbo_gpu, cudaGraphicsMapFlagsWriteDiscard);*/

	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	// warm up call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void uninitialize(void)
{
	if (gbFullScreen == TRUE) {
		toggleFullScreen();
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}

	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (vbo_cpu)
	{
		glDeleteBuffers(1, &vbo_cpu);
		vbo_cpu = 0;
	}

	if (vbo_gpu)
	{
		glDeleteBuffers(1, &vbo_gpu);
		vbo_gpu = 0;
	}

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShederObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShederObject);
	gFragmentShederObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	if (pos)
	{
		delete(pos);
		pos = NULL;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (pos == NULL)
	{
		if (vbo_gpu)
		{
			glDeleteBuffers(1, &vbo_gpu);
			vbo_gpu = 0;
		}

		glGenBuffers(1, &vbo_gpu);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_gpu);
		glBufferData(GL_ARRAY_BUFFER, MYARRAYSIZE * sizeof(float), NULL, GL_DYNAMIC_DRAW);

		error = cudaGraphicsGLRegisterBuffer(&graphicsResource, vbo_gpu, cudaGraphicsMapFlagsWriteDiscard);

		pos = (float*)malloc((glMeshWidth * glMeshHeight * 4) * sizeof(float));

		int count = 0;
		// zero out the position array
		for (int i = 0; i < glMeshWidth; i++)
		{
			for (int j = 0; j < glMeshHeight; j++)
			{
				for (int k = 0; k < 4; ++k)
				{
					//*(pos + i + j + k) = 0;
					pos[count++] = 0;
				}
			}
		}
	}

	glUseProgram(gShaderProgramObject);	

	// declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	// initialization of above matrix to identity
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = mat4::identity();

	// do matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	// send necessary matrices to shaders in respective uniforms
	// mvpUniform = glGetUniformLocation(...) 
	// so mvpUniform on CPU and u_mvp_matrix is on GPU
	// transpose means interchanging matrix row and column which is required in direct-x 
	// but not in open-gl so GL_FALSE
	glUniformMatrix4fv(mvpUniform, 1 /* 1 array */, GL_FALSE, modelViewProjectionMatrix);

	// rebind with vao
	glBindVertexArray(vao);

	if (bOnGPU == true)
	{
		error = cudaGraphicsMapResources(1, &graphicsResource, 0);

		if (error == cudaSuccess)
		{
			float4* pPos = NULL;
			size_t byteCount;
			error = cudaGraphicsResourceGetMappedPointer((void**)&pPos,
				&byteCount, graphicsResource);

			glBindBuffer(GL_ARRAY_BUFFER, vbo_gpu);
			launchCudaKernel(pPos, glMeshWidth, glMeshHeight, animationTime);

			glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

			error = cudaGraphicsUnmapResources(1, &graphicsResource);
		}
	}
	else
	{
		launchCPUKernel(glMeshWidth, glMeshHeight, animationTime);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_cpu);
		glBufferData(GL_ARRAY_BUFFER, MYARRAYSIZE * sizeof(float), pos, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	}	

	// similarly bind with textures if any

	// draw necessary scene
	glDrawArrays(GL_POINTS, 0, glMeshWidth * glMeshHeight);

	// unbind vao
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
	animationTime += 0.01f;

	if (animationTime > 360.0f)
		animationTime = 0.0f;
}

void launchCPUKernel(unsigned int meshWidth, unsigned int meshHeight, float animationTime)
{
	int count = 0;
	for (int i = 0; i < meshWidth; i++)
	{
		for (int j = 0; j < meshHeight; j++)
		{
			for (int k = 0; k < 4; k++)
			{
				float u = i / (float)meshWidth;
				float v = j / (float)meshHeight;
				u = u * 2.0 - 1.0;
				v = v * 2.0 - 1.0;
				float frequency = 4.0;
				float w = sinf(frequency * u + animationTime) *
					cosf(frequency * v + animationTime) * 0.5;
				if (k == 0)
				{
					*(pos + count) = u;
					//pos[i][j][k] = u;
				}
				if (k == 1)
				{
					*(pos + count) = w;
					//pos[i][j][k] = w;
				}
				if (k == 2)
				{
					*(pos + count) = v;
					//pos[i][j][k] = v;
				}
				if (k == 3)
				{
					*(pos + count) = 1.0;
					//pos[i][j][k] = 1.0;
				}
				count++;
			}
		}
	}
}