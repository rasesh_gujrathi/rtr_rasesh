// nvcc -o pp_cuda_sinewave.dll --shared pp_cuda_sinewave.cu
// nvcc -c pp_cuda_sinewave.cu -o pp_cuda_sinewave.obj

#include <stdio.h>
#include <cuda.h> // for CUDA
#include "pp_cuda_sinewave.h"

__global__ void sinewave_vbo_kernel(float4* pos, unsigned int width, unsigned int height, float animationTime)
{
	unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;

	float u = x / (float)width;
	float v = y / (float)height;

	u = u * 2.0f - 1.0f;
	v = v * 2.0f - 1.0f;

	float frequency = 4.0f;

	float w = sinf(frequency * u + animationTime) * cosf(frequency * v + animationTime) * 0.5f;

	pos[y * width + x] = make_float4(u, w, v, 1.0f);
}

//extern "C" void __declspec(dllexport) launchCudaKernel(float4* pos, unsigned int meshWidth, unsigned int meshHeight, float time)
extern "C" void launchCudaKernel(float4 * pos, unsigned int meshWidth, unsigned int meshHeight, float time)
{
	dim3 block(8, 8, 1);
	dim3 grid(meshWidth / block.x, meshHeight / block.y, 1);
	sinewave_vbo_kernel << <grid, block >> > (pos, meshWidth, meshHeight, time);
}
