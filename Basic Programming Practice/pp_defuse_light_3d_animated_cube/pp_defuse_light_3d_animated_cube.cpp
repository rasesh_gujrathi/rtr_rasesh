#include <Windows.h>
#include <assert.h>
#include <stdio.h>
#include <GL/glew.h> // added for programmable pipeline
#include <gl/GL.h>
#include "vmath.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib") // added for programmable pipeline for glew dependency

#define WIN_WIDTH 1024
#define WIN_HEIGHT 768

using namespace vmath;

static float angleCube = 0; // initializes cube's angle to 0.0f

mat4 rotationMatrix = mat4::identity();
BOOL bLight = TRUE;
BOOL bAnimate = FALSE;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCORD
};

// Global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL; // open-gl rendering context
HWND gHwnd = NULL;
DWORD dw_style;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

GLuint vao_rectangle;

// for position
GLuint vbo_rectangle;

// for normals
GLuint vbo_normals_rectangle;

// light uniforms
GLuint modelViewMatrixUniform;
GLuint projectionMatrixUniform;
GLuint ldUniform;
GLuint kdUniform;
GLuint lightPositionUniform;
GLuint lKeyPressedUniform;

mat4 perspectiveProjectionMatrix;

GLint gVertexShaderObject;
GLint gFragmentShederObject;
GLint gShaderProgramObject;

bool gbActiveWindow = false;
bool gbFullScreen = false;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void toggleFullScreen(void);
void resize(int width, int height);
void uninitialize(void);
void display(void);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void update();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	int initialize(void);
	int iRet = 0;
	bool bDone = false;

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("Programmable pipeline - Defuse light on 3d rotating cube!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS |
		WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	assert(hwnd);

	iRet = initialize();

	if (iRet < 0)
	{
		// error handling
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (bAnimate)
				{
					update();
				}
			}
			display();
		}
	}

	return((int)msg.wParam);
}

int width = 0, height = 0;

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void toggleFullScreen(void);

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		gHwnd = hwnd;
		break;
	case WM_SETFOCUS: gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: gbActiveWindow = false;
		break;
	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(width, height);
		break;
	case WM_CHAR:
		switch (wParam)
		{
			// a, A key
		case 65:
		case 97:
			if (bAnimate == false)
			{
				bAnimate = true;
			}
			else
			{
				bAnimate = false;
			}
			break;
		case 70:
		case 102: toggleFullScreen();
			break;
		case 27:
			DestroyWindow(hwnd);
			break;
			// l, L KEY
		case 76:
		case 108:
			if (bLight == false)
			{
				bLight = true;
			}
			else
			{
				bLight = false;
			}
			break;
		}
		break;
	case WM_ERASEBKGND:
		return (0);
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void toggleFullScreen(void)
{
	MONITORINFO mI;

	if (gbFullScreen == false)
	{
		dw_style = GetWindowLong(gHwnd, GWL_STYLE);

		if (dw_style & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev))
			{
				HMONITOR hMonitor = MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY);
				if (hMonitor)
				{
					mI.cbSize = sizeof(MONITORINFO);
					if (GetMonitorInfo(hMonitor, &mI))
					{
						SetWindowLong(gHwnd, GWL_STYLE, dw_style & ~WS_OVERLAPPEDWINDOW);

						SetWindowPos(gHwnd,
							HWND_TOP,
							mI.rcMonitor.left,
							mI.rcMonitor.top,
							mI.rcMonitor.right - mI.rcMonitor.left,
							mI.rcMonitor.bottom - mI.rcMonitor.top,
							SWP_NOZORDER | SWP_FRAMECHANGED);
					}
				}
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else // if already full screen
	{
		SetWindowLong(gHwnd, GWL_STYLE, dw_style | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

int initialize(void)
{
	GLenum result;

	// method declarations
	void resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	// initialize pfd structure

	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	result = glewInit(); // enable all graphic library extensions i.e. core profile (only programmable pipeline)

	if (result != GLEW_OK)
	{
		uninitialize();
	}

	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar* vertexShaderSourceCode = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_modelViewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_kd;" \
		"uniform vec4 u_light_position;" \
		"out vec3 defuse_color;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_cordinates = u_modelViewMatrix * vPosition;" \
		"mat3 normalMatrix = mat3(transpose(inverse(u_modelViewMatrix)));" \
		"vec3 transformedNormal = normalize(normalMatrix * vNormal);" \
		"vec3 s = normalize(vec3(u_light_position) - eye_cordinates.xyz);" \
		"defuse_color = u_ld * u_kd * dot(s, transformedNormal);" \
		"gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition;" \
		"}" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLen = 0;
	char* szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// define fragment shader object
	gFragmentShederObject = glCreateShader(GL_FRAGMENT_SHADER);

	// fragment shader code
	const GLchar* fragmentShaderSourceCode = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"in vec3 defuse_color;" \
		"uniform int u_lKeyIsPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"FragColor = vec4(defuse_color, 1.0);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}" \
		"}";

	glShaderSource(gFragmentShederObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gFragmentShederObject);

	iShaderCompileStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetShaderiv(gFragmentShederObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gFragmentShederObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShederObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Create shader program object
	gShaderProgramObject = glCreateProgram();

	// Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShederObject);

	// prelinking - binding to vertex attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link the shader program
	glLinkProgram(gShaderProgramObject);

	// error handling
	GLint iProgramLinkStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		// compilation failed
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	modelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelViewMatrix");
	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	const GLfloat cubeVertices[] =
	{
	-1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, -1.0f,

	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, -1.0f,

	-1.0f, 1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,

	-1.0f, 1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, 1.0f, -1.0f,

	1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, 1.0f, -1.0f,

	-1.0f, 1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f
	};

	const GLfloat cubeVerticesNormals[] =
	{
	1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f,

	1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,

	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f,

	1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f, 1.0f, -1.0f,
	1.0f, 1.0f, -1.0f,

	1.0f, 1.0f, -1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, -1.0f,

	-1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f, 0.0f,
	-1.0f, 0.0f, 0.0f
	};

	// create vao for rectangle model
	glGenVertexArrays(1, &vao_rectangle);
	glBindVertexArray(vao_rectangle);

	glGenBuffers(1, &vbo_rectangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_rectangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_normals_rectangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normals_rectangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVerticesNormals), cubeVerticesNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0); // unbound (rebind in display)

	// glEnable(GL_CULL_FACE); // disables model's backside rendering
	// glDisable(GL_CULL_FACE); // default : enables model's backside rendering

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	// warm up call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void uninitialize(void)
{
	if (gbFullScreen == TRUE) {
		toggleFullScreen();
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}

	if (vao_rectangle)
	{
		glDeleteVertexArrays(1, &vao_rectangle);
		vao_rectangle = 0;
	}

	if (vbo_rectangle)
	{
		glDeleteBuffers(1, &vbo_rectangle);
		vbo_rectangle = 0;
	}

	if (vbo_normals_rectangle)
	{
		glDeleteBuffers(1, &vbo_normals_rectangle);
		vbo_normals_rectangle = 0;
	}

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShederObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShederObject);
	gFragmentShederObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	// declaration of matrices
	mat4 modelViewMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	modelViewMatrix = translate(0.5f, 0.0f, -6.0f);
	rotationMatrix = rotate(angleCube, 1.0f, 0.0f, 0.0f);

	// do matrix multiplication
	// TRS - transform, rotate, scale
	modelViewMatrix = modelViewMatrix * rotationMatrix; // 2nd tRs

	glUniformMatrix4fv(modelViewMatrixUniform, 1 /* 1 array */, GL_FALSE, modelViewMatrix);

	glUniformMatrix4fv(projectionMatrixUniform, 1 /* 1 array */, GL_FALSE, perspectiveProjectionMatrix);

	GLfloat lightPosition[] = { 0.0f, 0.0f, 2.0f, 1.0f };

	if (bLight == TRUE)
	{
		glUniform1i(lKeyPressedUniform, 1);
		glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f); // white light
		glUniform3f(kdUniform, 0.5f, 0.5f, 0.5f); // gray, material
		glUniform4fv(lightPositionUniform, 1, lightPosition); // equivalent to glLightfv in ffp
	}
	else
	{
		glUniform1i(lKeyPressedUniform, 0);
	}

	// rebind with vao_rectangle
	glBindVertexArray(vao_rectangle);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); // GL_BEGIN
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4); // GL_BEGIN
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4); // GL_BEGIN
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4); // GL_BEGIN
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4); // GL_BEGIN
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4); // GL_BEGIN

	// unbind vao_rectangle
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

void update(void)
{
	angleCube += 0.5;

	if (angleCube >= 360.0f)
		angleCube = 0.0f;
}