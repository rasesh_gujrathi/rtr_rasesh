#include <Windows.h>
#include <assert.h>
#include <stdio.h>
#include <GL/glew.h>	// added for programmable pipeline
#include <gl/GL.h>
#include "vmath.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")	// added for programmable pipeline for glew dependency

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCORD
};

// Global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// open-gl rendering context
HWND gHwnd = NULL;
DWORD dw_style;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
FILE* gpFile = NULL;
bool gbActiveWindow = false;
bool gbFullScreen = false;
bool gbEscapeKeyIsPressed = false;

GLuint vao;
GLuint vbo;
GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;

GLuint gVertexShaderObject;
GLuint gTessellationControlShaderObject;
GLuint gTessellationEvaluationShaderObject;
GLuint gFragmentShederObject;
GLuint gShaderProgramObject;
GLuint gNumberOfSegmentUniform;
GLuint gNumberOfStripsUniform;
GLuint gLineColorUniform;

unsigned int gNumberOfLineSegments;

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void toggleFullScreen(void);
void resize(int width, int height);
void uninitialize(void);
void display(void);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OpenGLPP");
	int initialize(void);
	int iRet = 0;
	bool bDone = false;


	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to create log file...\nExiting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file created successfully...");
	}

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("Programmable pipeline - Tessellation Shader!"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS |
		WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	assert(hwnd);

	iRet = initialize();

	if (iRet < 0)
	{
		// error handling
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true) // Continuation to glutLeaveMainLoop()
					bDone = true;
			}
			display();
		}
	}

	return((int)msg.wParam);
}

int width = 0, height = 0;

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declaration
	void toggleFullScreen(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) /// if 0, the window is active
			gbActiveWindow = true;
		else
		{
			// if non-zero, the windiw is non active
		}
		gbActiveWindow = false;
		break;
	case WM_CREATE:
		gHwnd = hwnd;
		break;
	case WM_SETFOCUS: gbActiveWindow = true;
		break;
	case WM_KILLFOCUS: gbActiveWindow = false;
		break;
	case WM_SIZE:
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		resize(width, height); // parallel to glutReshapeFunc(resize) function
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_UP:
			gNumberOfLineSegments++;
			if (gNumberOfLineSegments >= 50)
				gNumberOfLineSegments = 50; // reset
			break;
		case VK_DOWN:
			gNumberOfLineSegments--;
			if (gNumberOfLineSegments <= 0)
				gNumberOfLineSegments = 1;	// reset
			break;
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;	// parallel to glutLeaveMainLoop();
			break;
		case 0x46:	// f or F
			if (gbFullScreen == false)
			{
				toggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				toggleFullScreen();
				gbFullScreen = false;
			}
			break;
		}
		break;
	case WM_ERASEBKGND:
		return (0);
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void toggleFullScreen(void)
{
	MONITORINFO mI;

	if (gbFullScreen == false)
	{
		dw_style = GetWindowLong(gHwnd, GWL_STYLE);

		if (dw_style & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &wpPrev))
			{
				HMONITOR hMonitor = MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY);
				if (hMonitor)
				{
					mI.cbSize = sizeof(MONITORINFO);
					if (GetMonitorInfo(hMonitor, &mI))
					{
						SetWindowLong(gHwnd, GWL_STYLE, dw_style & ~WS_OVERLAPPEDWINDOW);

						SetWindowPos(gHwnd,
							HWND_TOP,
							mI.rcMonitor.left,
							mI.rcMonitor.top,
							mI.rcMonitor.right - mI.rcMonitor.left,
							mI.rcMonitor.bottom - mI.rcMonitor.top,
							SWP_NOZORDER | SWP_FRAMECHANGED);
					}
				}
			}
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else // if already full screen 
	{
		SetWindowLong(gHwnd, GWL_STYLE, dw_style | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(gHwnd, &wpPrev);

		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

int initialize(void)
{
	GLenum result;

	// method declarations
	void resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	// initialize pfd structure

	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	result = glewInit();	// enable all graphic library extensions i.e. core profile (only programmable pipeline)

	if (result != GLEW_OK)
	{
		uninitialize();
	}

	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar* vertexShaderSourceCode = (const GLchar*)
		"#version 450" \
		"\n" \
		"in vec2 vPosition;" \
		"void main(void)" \
		"{" \
		"gl_Position = vec4(vPosition, 0.0, 1.0);" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLen = 0;
	char* szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLen, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Tessellation control shader
	// create shader
	gTessellationControlShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);

	// provide source code to shader
	const GLchar* tessellationControlShaderSourceCode =
		"#version 450" \
		"\n" \
		"layout(vertices=4)out;" \
		"uniform int numberOfSegments;" \
		"uniform int numberOfStrips;" \
		"void main(void)" \
		"{" \
		"gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" \
		"gl_TessLevelOuter[0] = float(numberOfStrips);" \
		"gl_TessLevelOuter[1] = float(numberOfSegments);" \
		"}";

	glShaderSource(gTessellationControlShaderObject, 1, (const GLchar**)&tessellationControlShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gTessellationControlShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLen = 0;
	szInfoLog = NULL;

	glGetShaderiv(gTessellationControlShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gTessellationControlShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gTessellationControlShaderObject, iInfoLogLen, &written, szInfoLog);
				fprintf(gpFile, "Tessellation Control Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Tessellation control shader
	// create shader
	gTessellationEvaluationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);

	// provide source code to shader
	const GLchar* tessellationEvaluationShaderSourceCode =
		"#version 450" \
		"\n" \
		"layout(isolines)in;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"float u = gl_TessCoord.x;" \
		"vec3 p0 = gl_in[0].gl_Position.xyz;" \
		"vec3 p1 = gl_in[1].gl_Position.xyz;" \
		"vec3 p2 = gl_in[2].gl_Position.xyz;" \
		"vec3 p3 = gl_in[3].gl_Position.xyz;" \
		"float u1 = (1.0 - u);" \
		"float u2 = u * u;" \
		"float b3 = u2 * u;" \
		"float b2 = 3.0 * u2 * u1;" \
		"float b1 = 3.0 * u * u1 * u1;" \
		"float b0 = u1 * u1 * u1;" \
		"vec3 p = p0 * b0 + p1 * b1 + p2 * b2 + p3 * b3;" \
		"gl_Position = u_mvp_matrix * vec4(p, 1.0);" \
		"}";

	glShaderSource(gTessellationEvaluationShaderObject, 1, (const GLchar**)&tessellationEvaluationShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gTessellationEvaluationShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLen = 0;
	szInfoLog = NULL;

	glGetShaderiv(gTessellationEvaluationShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gTessellationEvaluationShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gTessellationEvaluationShaderObject, iInfoLogLen, &written, szInfoLog);
				fprintf(gpFile, "Tessellation Evaluation Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// define fragment shader object
	gFragmentShederObject = glCreateShader(GL_FRAGMENT_SHADER);

	// fragment shader code
	const GLchar* fragmentShaderSourceCode = (const GLchar*)
		"#version 450" \
		"\n" \
		"uniform vec4 lineColor;" \
		"out vec4 FragColor;"
		"void main(void)" \
		"{" \
		"FragColor = lineColor;" \
		"}";

	glShaderSource(gFragmentShederObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gFragmentShederObject);

	iShaderCompileStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetShaderiv(gFragmentShederObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gFragmentShederObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShederObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Create shader program object
	gShaderProgramObject = glCreateProgram();

	// Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// Attach tessellation control shader to the shader program
	glAttachShader(gShaderProgramObject, gTessellationControlShaderObject);

	// Attach tessellation evaluation shader to the shader program
	glAttachShader(gShaderProgramObject, gTessellationEvaluationShaderObject);

	// Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShederObject);

	// prelinking - binding to vertex attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	// Link the shader program
	glLinkProgram(gShaderProgramObject);

	// error handling
	GLint iProgramLinkStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		// compilation failed
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	gNumberOfSegmentUniform = glGetUniformLocation(gShaderProgramObject, "numberOfSegments");
	gNumberOfStripsUniform = glGetUniformLocation(gShaderProgramObject, "numberOfStrips");
	gLineColorUniform = glGetUniformLocation(gShaderProgramObject, "lineColor");

	float vertices[] = { -1.0f, -1.0f, -0.5f, 1.0f, 0.5f, -1.0f, 1.0f, 1.0f };

	// create vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);	// unbound (rebind in display)

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);
	glLineWidth(3.0f);

	perspectiveProjectionMatrix = mat4::identity();
	gNumberOfLineSegments = 1;
	// warm up call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void uninitialize(void)
{
	if (gbFullScreen == TRUE) {
		toggleFullScreen();
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc);
		ghdc = NULL;
	}

	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}

	glDetachShader(gShaderProgramObject, gFragmentShederObject);
	glDetachShader(gShaderProgramObject, gTessellationControlShaderObject);
	glDetachShader(gShaderProgramObject, gTessellationEvaluationShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	glDeleteShader(gFragmentShederObject);
	gFragmentShederObject = 0;

	glDeleteShader(gTessellationControlShaderObject);
	gTessellationControlShaderObject = 0;

	glDeleteShader(gTessellationEvaluationShaderObject);
	gTessellationEvaluationShaderObject = 0;

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	if (gpFile)
	{
		fprintf(gpFile, "Closing the log file\n");
		fclose(gpFile);
		gpFile = NULL;
	}

	glUseProgram(0);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	// declaration of matrices
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	// initialization of above matrix to identity
	//modelViewMatrix = translate(0.5f, 0.5f, -2.0f);
	modelViewMatrix = translate(0.0f, 0.0f, -4.0f);

	// do necessary transformation

	// do matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	// send necessary matrices to shaders in respective uniforms
	// mvpUniform = glGetUniformLocation(...) 
	// so mvpUniform on CPU and u_mvp_matrix is on GPU
	// transpose means interchanging matrix row and column which is required in direct-x 
	// but not in open-gl so GL_FALSE
	glUniformMatrix4fv(mvpUniform, 1 /* 1 array */, GL_FALSE, modelViewProjectionMatrix);

	// send other uniforms
	glUniform1i(gNumberOfSegmentUniform, gNumberOfLineSegments);

	TCHAR str[255];
	wsprintf(str, TEXT("OpenGL Programmable Pipeline Window : [ Segments = %d ]"), gNumberOfLineSegments);

	SetWindowText(gHwnd, str);
	glUniform1i(gNumberOfStripsUniform, 1);

	if (gNumberOfLineSegments == 1)
	{
		glUniform4fv(gLineColorUniform, 1, vmath::vec4(1.0f, 1.0f, 0.0f, 1.0f));
	}
	else
		if (gNumberOfLineSegments >= 50)
		{
			glUniform4fv(gLineColorUniform, 1, vmath::vec4(0.0f, 1.0f, 0.0f, 1.0f));
		}
		else
		{
			glUniform4fv(gLineColorUniform, 1, vmath::vec4(1.0f, 0.0f, 0.0f, 1.0f));
		}

	// rebind with vao
	glBindVertexArray(vao);
	glPatchParameteri(GL_PATCH_VERTICES, 4);
	// similarly bind with textures if any

	// draw necessary scene
	glDrawArrays(GL_PATCHES, 0, 4);		// GL_BEGIN

	// unbind vao
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

