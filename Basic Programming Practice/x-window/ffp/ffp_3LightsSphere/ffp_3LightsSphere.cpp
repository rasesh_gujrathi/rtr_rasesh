// g++ -o ffp_3LightsSphere.o ffp_3LightsSphere.cpp -L/usr/lib/x86_64-linux-gnu -lX11 -lGL -lGLU

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

// xlib headers
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

// opengl headers
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

// namespaces
using namespace std;

// global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
static GLXContext gGlxContex;
GLUquadric* quadric = NULL;

void update(void);

// lights
typedef struct Light
{
	GLfloat ambient[4] = { 0.0f };
	GLfloat deffuse[4] = { 0.0f };
	GLfloat specular[4] = { 0.0f };
	GLfloat position[4] = { 0.0f };
	GLfloat angle;
} GLLight;

bool bLight = false;
GLfloat lightAmbientZero[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat lightDiffuseZero[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightSpecularZero[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightPositionZero[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

GLfloat lightAmbientOne[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat lightDiffuseOne[4] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat lightSpecularOne[4] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat lightPositionOne[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

GLfloat lightAmbientTwo[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat lightDiffuseTwo[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat lightSpecularTwo[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat lightPositionTwo[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

GLLight lights[3];

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat materialDiffuse[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat materialPosition[] = { 2.0f, 0.0f, 0.0f, 1.0f };
GLfloat materialShininess[] = { 50.0f };

// entry point function
int main(void)
{
    // function prototypes
    void CreateWindow();
    void ToggleFullscreen(void);
    void uninitialize();
    void initialize(void);
    void resize(int, int);
    void display(void);

    // variable declarations
    static int winWidth = giWindowWidth;
    static int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;

    // code
    CreateWindow();

    initialize();

    // Message loop
    XEvent event;
    KeySym keysum;

    GC gc;
    XGCValues gcValues;
    XColor text_color;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify: 
                break;
                case KeyPress: 
                    keysum = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysum)
                    {
                        case XK_Escape: bDone = true;               
                        break;
                        default: break;                    
                    }//switch
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch(keys[0])
                    {
                        case 'l':
                        case 'L':
                        lights[0].angle = 0.0f;
                        lights[1].angle = 0.0f;
                        lights[2].angle = 0.0f;

                        if (bLight == false)
                        {
                            bLight = true;
                            glEnable(GL_LIGHTING);
                        }
                        else
                        {
                            bLight = false;
                            glDisable(GL_LIGHTING);
                        }
                        break;
                        case 'F':
                        case 'f':
                            ToggleFullscreen();
                            if(bFullScreen == false)
                            {
                                bFullScreen = true;            
                            }
                            else
                            {
                                bFullScreen = false;            
                            }
                            break;
                    }
                break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1: break;  // left
                        case 2: break;  // middle
                        case 3: break;  // right
                        case 4: break;  // mouse wheel up
                        case 5: break;  // mouse wheel down
                        default: break;
                    }
                break;
                case MotionNotify: break;
                case ConfigureNotify: 
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth, winHeight);
                break;
                case Expose:
                break;
                case DestroyNotify:
                break;
                case 33:    // window cross ('x') pressed to close window
                    XFreeGC(gpDisplay, gc);
                    bDone = true;
                default: break;
            }//switch        
        }//while(xPending(gpDisplay))
        display();
        update();
    }//while(bDone == false)

    uninitialize();
    return 0;
}

void CreateWindow(void)
{
    // function prototype
    void uninitialize();

    // variable declarations
    XSetWindowAttributes winAttribes;
    int defaultScreen;
    int styleMask;

    static int frameBufferAttributes[] = 
    {
        GLX_RGBA,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);

    if(gpDisplay == NULL)
    {
        printf("Error : unable to open x display.\nExisting now...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: unable to allocate memory for visual info.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XMatchVisualInfo (gpDisplay, defaultScreen, 0, TrueColor, gpXVisualInfo);

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to get a visual.\n Exiting now...\n");
        uninitialize();
        exit(1);
    }

    winAttribes.border_pixel = 0;
    winAttribes.border_pixmap = 0;
    winAttribes.background_pixmap = 0;
    winAttribes.colormap = XCreateColormap(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    gpXVisualInfo->visual, AllocNone);

    gColormap = winAttribes.colormap;
    winAttribes.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribes.event_mask = ExposureMask | VisibilityChangeMask |
    ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
    0, 0, giWindowWidth, giWindowHeight, 0, gpXVisualInfo->depth, InputOutput,
    gpXVisualInfo->visual, styleMask, &winAttribes);

    if(!gWindow)
    {
        printf("ERROR : Failed to create main window.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "3 lights sphere!");
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = { 0 };

    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));
    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    False,
    StructureNotifyMask,
    &xev);
}

void uninitialize()
{
    GLXContext currentGlxContext = glXGetCurrentContext();

    if(currentGlxContext != NULL && currentGlxContext == gGlxContex)
    {
        glXMakeCurrent(gpDisplay, 0, 0);

        if(gGlxContex)
        {
            glXDestroyContext(gpDisplay, gGlxContex);
        }
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

void initialize(void)
{
    GLenum result;
    // method declarations
	void resize(int, int);
    void defineLights();

	defineLights();

    gGlxContex = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

    glXMakeCurrent(gpDisplay, gWindow, gGlxContex);
	
    glLightfv(GL_LIGHT0, GL_AMBIENT, lights[0].ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lights[0].deffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lights[0].specular);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1, GL_AMBIENT, lights[1].ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lights[1].deffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lights[1].specular);
	glEnable(GL_LIGHT1);

	glLightfv(GL_LIGHT2, GL_AMBIENT, lights[2].ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, lights[2].deffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, lights[2].specular);
	glEnable(GL_LIGHT2);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// warm up call to resize
	resize(giWindowWidth, giWindowHeight);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -0.70f);

	glPushMatrix();
	gluLookAt(0.0f, 0.0f, 0.2f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	// light 0	

	glPushMatrix();
	glRotatef(lights[0].angle, 1.0f, 0.0f, 0.0f);
	lights[0].position[1] = lights[0].angle;
	glLightfv(GL_LIGHT0, GL_POSITION, lights[0].position);
	glPopMatrix();

	// light 1
	glPushMatrix();
	glRotatef(lights[1].angle, 0.0f, 1.0f, 0.0f);
	lights[1].position[0] = lights[1].angle;
	glLightfv(GL_LIGHT1, GL_POSITION, lights[1].position);
	glPopMatrix();

	// light 2
	glPushMatrix();
	glRotatef(lights[2].angle, 0.0f, 0.0f, 1.0f);
	lights[2].position[0] = lights[2].angle;
	glLightfv(GL_LIGHT2, GL_POSITION, lights[2].position);
	glPopMatrix();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30);

	glPopMatrix();
	
	glXSwapBuffers(gpDisplay, gWindow);
}

void update(void)
{
	lights[0].angle += 0.5f;

	if (lights[0].angle >= 360.0f)
		lights[0].angle = 0.0f;

	lights[1].angle += 0.5f;

	if (lights[1].angle >= 360.0f)
		lights[1].angle = 0.0f;

	lights[2].angle += 0.5f;

	if (lights[2].angle >= 360.0f)
		lights[2].angle = 0.0f;
}

void defineLights()
{
	// 1st light
	lights[0].ambient[0] = lightAmbientZero[0];
	lights[0].ambient[1] = lightAmbientZero[1];
	lights[0].ambient[2] = lightAmbientZero[2];
	lights[0].ambient[3] = lightAmbientZero[3];

	lights[0].deffuse[0] = lightDiffuseZero[0];
	lights[0].deffuse[1] = lightDiffuseZero[1];
	lights[0].deffuse[2] = lightDiffuseZero[2];
	lights[0].deffuse[3] = lightDiffuseZero[3];

	lights[0].specular[0] = lightSpecularZero[0];
	lights[0].specular[1] = lightSpecularZero[1];
	lights[0].specular[2] = lightSpecularZero[2];
	lights[0].specular[3] = lightSpecularZero[3];

	lights[0].position[0] = lightPositionZero[0];
	lights[0].position[1] = lightPositionZero[1];
	lights[0].position[2] = lightPositionZero[2];
	lights[0].position[3] = lightPositionZero[3];

	// 2nd light
	lights[1].ambient[0] = lightAmbientOne[0];
	lights[1].ambient[1] = lightAmbientOne[1];
	lights[1].ambient[2] = lightAmbientOne[2];
	lights[1].ambient[3] = lightAmbientOne[3];

	lights[1].deffuse[0] = lightDiffuseOne[0];
	lights[1].deffuse[1] = lightDiffuseOne[1];
	lights[1].deffuse[2] = lightDiffuseOne[2];
	lights[1].deffuse[3] = lightDiffuseOne[3];

	lights[1].specular[0] = lightSpecularOne[0];
	lights[1].specular[1] = lightSpecularOne[1];
	lights[1].specular[2] = lightSpecularOne[2];
	lights[1].specular[3] = lightSpecularOne[3];

	lights[1].position[0] = lightPositionOne[0];
	lights[1].position[1] = lightPositionOne[1];
	lights[1].position[2] = lightPositionOne[2];
	lights[1].position[3] = lightPositionOne[3];

	// 3rd light
	lights[2].ambient[0] = lightAmbientTwo[0];
	lights[2].ambient[1] = lightAmbientTwo[1];
	lights[2].ambient[2] = lightAmbientTwo[2];
	lights[2].ambient[3] = lightAmbientTwo[3];

	lights[2].deffuse[0] = lightDiffuseTwo[0];
	lights[2].deffuse[1] = lightDiffuseTwo[1];
	lights[2].deffuse[2] = lightDiffuseTwo[2];
	lights[2].deffuse[3] = lightDiffuseTwo[3];

	lights[2].specular[0] = lightSpecularTwo[0];
	lights[2].specular[1] = lightSpecularTwo[1];
	lights[2].specular[2] = lightSpecularTwo[2];
	lights[2].specular[3] = lightSpecularTwo[3];

	lights[2].position[0] = lightPositionTwo[0];
	lights[2].position[1] = lightPositionTwo[1];
	lights[2].position[2] = lightPositionTwo[2];
	lights[2].position[3] = lightPositionTwo[3];
}
