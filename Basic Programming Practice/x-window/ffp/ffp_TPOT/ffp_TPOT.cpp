// g++ -o ffp_TPOT.o ffp_TPOT.cpp -L/usr/lib/x86_64-linux-gnu -lX11 -lGL -lGLU -lSOIL

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

// xlib headers
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

// opengl headers
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
#include <SOIL/SOIL.h>
#include "OGL.h"

// namespaces
using namespace std;

// global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
static GLXContext gGlxContex;
static float angle = 360.0f;

// lights
bool bLight = false;
GLfloat lightAmbient[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 1.0f };

void update(void);

GLuint textureMarble;
// entry point function
int main(void)
{
    // function prototypes
    void CreateWindow();
    void ToggleFullscreen(void);
    void uninitialize();
    void initialize(void);
    void resize(int, int);
    void display(void);

    // variable declarations
    static int winWidth = giWindowWidth;
    static int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;

    // code
    CreateWindow();

    initialize();

    // Message loop
    XEvent event;
    KeySym keysum;

    GC gc;
    XGCValues gcValues;
    XColor text_color;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify: 
                break;
                case KeyPress: 
                    keysum = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysum)
                    {
                        case XK_Escape: bDone = true;               
                        break;
                        default: break;                    
                    }//switch
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch(keys[0])
                    {
                        case 'l':
                        case 'L':
                            if (bLight == false)
                            {
                                bLight = true;
                                glEnable(GL_LIGHTING);
                            }
                            else
                            {
                                bLight = false;
                                glDisable(GL_LIGHTING);
                            }
                            break;
                        case 'F':
                        case 'f':
                            ToggleFullscreen();
                            if(bFullScreen == false)
                            {
                                bFullScreen = true;            
                            }
                            else
                            {
                                bFullScreen = false;            
                            }
                            break;
                    }
                break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1: break;  // left
                        case 2: break;  // middle
                        case 3: break;  // right
                        case 4: break;  // mouse wheel up
                        case 5: break;  // mouse wheel down
                        default: break;
                    }
                break;
                case MotionNotify: break;
                case ConfigureNotify: 
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth, winHeight);
                break;
                case Expose:
                break;
                case DestroyNotify:
                break;
                case 33:    // window cross ('x') pressed to close window
                    XFreeGC(gpDisplay, gc);
                    bDone = true;
                default: break;
            }//switch        
        }//while(xPending(gpDisplay))
        display();
        update();
    }//while(bDone == false)

    uninitialize();
    return 0;
}

void CreateWindow(void)
{
    // function prototype
    void uninitialize();

    // variable declarations
    XSetWindowAttributes winAttribes;
    int defaultScreen;
    int styleMask;

    static int frameBufferAttributes[] = 
    {
        GLX_RGBA,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);

    if(gpDisplay == NULL)
    {
        printf("Error : unable to open x display.\nExisting now...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: unable to allocate memory for visual info.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XMatchVisualInfo (gpDisplay, defaultScreen, 0, TrueColor, gpXVisualInfo);

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to get a visual.\n Exiting now...\n");
        uninitialize();
        exit(1);
    }

    winAttribes.border_pixel = 0;
    winAttribes.border_pixmap = 0;
    winAttribes.background_pixmap = 0;
    winAttribes.colormap = XCreateColormap(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    gpXVisualInfo->visual, AllocNone);

    gColormap = winAttribes.colormap;
    winAttribes.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribes.event_mask = ExposureMask | VisibilityChangeMask |
    ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
    0, 0, giWindowWidth, giWindowHeight, 0, gpXVisualInfo->depth, InputOutput,
    gpXVisualInfo->visual, styleMask, &winAttribes);

    if(!gWindow)
    {
        printf("ERROR : Failed to create main window.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "TPOT Texture with light!");
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = { 0 };

    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));
    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    False,
    StructureNotifyMask,
    &xev);
}

void uninitialize()
{
    GLXContext currentGlxContext = glXGetCurrentContext();

    if(currentGlxContext != NULL && currentGlxContext == gGlxContex)
    {
        glXMakeCurrent(gpDisplay, 0, 0);

        if(gGlxContex)
        {
            glXDestroyContext(gpDisplay, gGlxContex);
        }
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

	glDeleteTextures(1, &textureMarble);
}

GLuint textureSmiley;

void initialize(void)
{
    GLenum result;
    // method declarations
	void resize(int, int);
    bool LoadTexture(GLuint *texture, const char* path);

    gGlxContex = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

    glXMakeCurrent(gpDisplay, gWindow, gGlxContex);
	
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glEnable(GL_LIGHT0);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_TEXTURE_2D);

    bool status = LoadTexture(&textureMarble, "marble.bmp");

    if(status == False)
    {
        exit(-1);
    }
	// warm up call to resize
	resize(giWindowWidth, giWindowHeight);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.5f);
	//glRotatef(90.0f, 1.0f, 0.0f, 0.0f); // x-axis rotation

	glRotatef(angle, 0.0f, 1.0f, 0.0f); // z-axis rotation
	glBindTexture(GL_TEXTURE_2D, textureMarble);

	glBegin(GL_TRIANGLES);

	for (int i = 0; i < sizeof(face_indicies) / sizeof(face_indicies[0]); i++)
	{
		for (int j = 0; j < 3; j++)
		{
			int vi = face_indicies[i][j];		// vertex
			int ni = face_indicies[i][j + 3];	// normals
			int ti = face_indicies[i][j + 6];	// texture

			glTexCoord2f(textures[ti][0], textures[ti][1]);
			glNormal3f(normals[ni][0], normals[ni][1], normals[ni][2]);
			glVertex3f(vertices[vi][0], vertices[vi][1], vertices[vi][2]);
		}
	}
	glEnd();

	glXSwapBuffers(gpDisplay, gWindow);
}

bool LoadTexture(GLuint *texture, const char* path)
{
    int imageWidth, imageHeight;
    bool bStatus = False;
    unsigned char* imageData = 0;
    imageData = SOIL_load_image(path, &imageWidth, &imageHeight, 0, SOIL_LOAD_RGB);

    if(imageData != 0)
    {
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

		// load texture in memory
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imageWidth, imageHeight, GL_RGB,
			GL_UNSIGNED_BYTE, imageData);
        
        bStatus = True;
        SOIL_free_image_data(imageData);
    }

    return bStatus;
}

void update(void)
{
	angle -= 1;

	if (angle <= 0.0f)
		angle = 360.0f;
}