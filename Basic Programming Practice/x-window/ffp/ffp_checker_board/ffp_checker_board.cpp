// g++ -o ffp_checker_board.o ffp_checker_board.cpp -L/usr/lib/x86_64-linux-gnu -lX11 -lGL -lGLU -lSOIL

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

// xlib headers
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

// opengl headers
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
#include <SOIL/SOIL.h>
// namespaces
using namespace std;

// global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
static GLXContext gGlxContex;

#define CHECKIMAGEWIDTH 64
#define CHECKIMAGEHEIGHT 64

GLubyte checkImage[CHECKIMAGEWIDTH][CHECKIMAGEHEIGHT][4];
GLuint texImage;
void LoadTexture(void);

// entry point function
int main(void)
{
    // function prototypes
    void CreateWindow();
    void ToggleFullscreen(void);
    void uninitialize();
    void initialize(void);
    void resize(int, int);
    void display(void);

    // variable declarations
    static int winWidth = giWindowWidth;
    static int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;

    // code
    CreateWindow();

    initialize();

    // Message loop
    XEvent event;
    KeySym keysum;

    GC gc;
    XGCValues gcValues;
    XColor text_color;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify: 
                break;
                case KeyPress: 
                    keysum = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysum)
                    {
                        case XK_Escape: bDone = true;               
                        break;
                        default: break;                    
                    }//switch
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch(keys[0])
                    {
                        case 'F':
                        case 'f':
                            ToggleFullscreen();
                            if(bFullScreen == false)
                            {
                                bFullScreen = true;            
                            }
                            else
                            {
                                bFullScreen = false;            
                            }
                            break;
                    }
                break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1: break;  // left
                        case 2: break;  // middle
                        case 3: break;  // right
                        case 4: break;  // mouse wheel up
                        case 5: break;  // mouse wheel down
                        default: break;
                    }
                break;
                case MotionNotify: break;
                case ConfigureNotify: 
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth, winHeight);
                break;
                case Expose:
                break;
                case DestroyNotify:
                break;
                case 33:    // window cross ('x') pressed to close window
                    XFreeGC(gpDisplay, gc);
                    bDone = true;
                default: break;
            }//switch        
        }//while(xPending(gpDisplay))
        display();
    }//while(bDone == false)

    uninitialize();
    return 0;
}

void CreateWindow(void)
{
    // function prototype
    void uninitialize();

    // variable declarations
    XSetWindowAttributes winAttribes;
    int defaultScreen;
    int styleMask;

    static int frameBufferAttributes[] = 
    {
        GLX_RGBA,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24,
        None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);

    if(gpDisplay == NULL)
    {
        printf("Error : unable to open x display.\nExisting now...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: unable to allocate memory for visual info.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XMatchVisualInfo (gpDisplay, defaultScreen, 0, TrueColor, gpXVisualInfo);

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to get a visual.\n Exiting now...\n");
        uninitialize();
        exit(1);
    }

    winAttribes.border_pixel = 0;
    winAttribes.border_pixmap = 0;
    winAttribes.background_pixmap = 0;
    winAttribes.colormap = XCreateColormap(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    gpXVisualInfo->visual, AllocNone);

    gColormap = winAttribes.colormap;
    winAttribes.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribes.event_mask = ExposureMask | VisibilityChangeMask |
    ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
    0, 0, giWindowWidth, giWindowHeight, 0, gpXVisualInfo->depth, InputOutput,
    gpXVisualInfo->visual, styleMask, &winAttribes);

    if(!gWindow)
    {
        printf("ERROR : Failed to create main window.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Checker board!");
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = { 0 };

    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));
    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    False,
    StructureNotifyMask,
    &xev);
}

void uninitialize()
{
    GLXContext currentGlxContext = glXGetCurrentContext();

    if(currentGlxContext != NULL && currentGlxContext == gGlxContex)
    {
        glXMakeCurrent(gpDisplay, 0, 0);

        if(gGlxContex)
        {
            glXDestroyContext(gpDisplay, gGlxContex);
        }
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

void initialize(void)
{
    GLenum result;
    // method declarations
	void resize(int, int);

    gGlxContex = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

    glXMakeCurrent(gpDisplay, gWindow, gGlxContex);
    glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_TEXTURE_2D);
	LoadTexture();

	// warm up call to resize
	resize(giWindowWidth, giWindowHeight);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.6f);

	glBegin(GL_QUADS);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-2.0f, -1.0f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-2.0f, 1.0f, 0.0f);
	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(2.41421f, 1.0f, -1.41421f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(2.41421f, -1.0f, -1.41421f);
	glEnd();
	
	glXSwapBuffers(gpDisplay, gWindow);
}

void LoadTexture(void)
{
	void MakeCheckImage(void);

	MakeCheckImage();

	glPixelStoref(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &texImage);
	glBindTexture(GL_TEXTURE_2D, texImage);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
		CHECKIMAGEWIDTH, CHECKIMAGEHEIGHT, 0,
		GL_RGBA, GL_UNSIGNED_BYTE,
		checkImage);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
} // LoadTexture

// procedural image creation using maths
void MakeCheckImage(void)
{
	int i, j, c = 0;

	for (i = 0; i < CHECKIMAGEHEIGHT; i++)
	{
		for (j = 0; j < CHECKIMAGEWIDTH; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;

			checkImage[i][j][0] = (GLubyte)c;
			checkImage[i][j][1] = (GLubyte)c;
			checkImage[i][j][2] = (GLubyte)c;
			checkImage[i][j][3] = 255;
		}// j
	}// i
}
