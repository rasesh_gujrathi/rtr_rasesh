// g++ -o ffp_triangle_multiviewport.o ffp_triangle_multiviewport.cpp -L/usr/lib/x86_64-linux-gnu -lX11 -lGL -lGLU

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

// xlib headers
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

// opengl headers
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

// namespaces
using namespace std;

// global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
static GLXContext gGlxContex;

void resize(GLint x, GLint y, int, int);

// entry point function
int main(void)
{
    // function prototypes
    void CreateWindow();
    void ToggleFullscreen(void);
    void uninitialize();
    void initialize(void);
    void display(void);

    // variable declarations
    static int winWidth = giWindowWidth;
    static int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;
    // code
    CreateWindow();

    initialize();
    
    // Message loop
    XEvent event;
    KeySym keysum;

    GC gc;
    XGCValues gcValues;
    XColor text_color;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify: 
                break;
                case KeyPress: 
                    keysum = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 1);
                    switch (keysum)
                    {
                        case XK_KP_0:           
                            resize(0, 0, winWidth, winHeight);
                            break;
                        case XK_KP_1:
                            resize(0, 0, winWidth / 2, winHeight / 2);	// left bottom
			                break;
                        case XK_KP_2:           
                            resize(winWidth / 2, 0, winWidth / 2, winHeight / 2);	// right bottom
			                break;
                        case XK_KP_3:           
                            resize(winWidth / 2, winHeight/2, winWidth / 2, winHeight / 2);	// right top
			                break;
                        case XK_KP_4:           
                            resize(0, winHeight / 2, winWidth / 2, winHeight / 2);	// left top
			                break;
                        case XK_KP_5:           
                            resize(0, 0, winWidth / 2, winHeight );	// left half
			                break;
                        case XK_KP_6:           
                            resize(winWidth/2, 0, winWidth / 2, winHeight);	// right half
			                break;
                        case XK_KP_7:           
                            resize(0, winHeight/2, winWidth, winHeight/2);	// top
			                break;
                        case XK_KP_8:           
                            resize(0, 0, winWidth, winHeight/2);	// bottom
			                break;
                        case XK_KP_9:           
                            resize(winWidth / 4, winHeight / 4, winWidth / 2, winHeight / 2);	// center
			                break;
                        // case XK_KP_Separator:
                        // case XK_KP_Decimal:     return GLFW_KEY_KP_DECIMAL;
                        // case XK_KP_Equal:       return GLFW_KEY_KP_EQUAL;
                        // case XK_KP_Enter:       return GLFW_KEY_KP_ENTER;
                        default:                
                            break;
                    }

                    keysum = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysum)
                    {
                        case XK_Escape: bDone = true;               
                        break;
                        default: break;                    
                    }//switch
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch(keys[0])
                    {
                        case 'F':
                        case 'f':
                            ToggleFullscreen();
                            if(bFullScreen == false)
                            {
                                bFullScreen = true;            
                            }
                            else
                            {
                                bFullScreen = false;            
                            }
                            break;
                    }
                break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1: break;  // left
                        case 2: break;  // middle
                        case 3: break;  // right
                        case 4: break;  // mouse wheel up
                        case 5: break;  // mouse wheel down
                        default: break;
                    }
                break;
                case MotionNotify: break;
                case ConfigureNotify: 
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(0, 0, winWidth, winHeight);
                break;
                case Expose:
                break;
                case DestroyNotify:
                break;
                case 33:    // window cross ('x') pressed to close window
                    XFreeGC(gpDisplay, gc);
                    bDone = true;
                default: break;
            }//switch        
        }//while(xPending(gpDisplay))
        display();
    }//while(bDone == false)

    uninitialize();
    return 0;
}

void CreateWindow(void)
{
    // function prototype
    void uninitialize();

    // variable declarations
    XSetWindowAttributes winAttribes;
    int defaultScreen;
    int styleMask;

    static int frameBufferAttributes[] = 
    {
        GLX_RGBA,
        GLX_DOUBLEBUFFER, True,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);

    if(gpDisplay == NULL)
    {
        printf("Error : unable to open x display.\nExisting now...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: unable to allocate memory for visual info.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XMatchVisualInfo (gpDisplay, defaultScreen, 0, TrueColor, gpXVisualInfo);

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to get a visual.\n Exiting now...\n");
        uninitialize();
        exit(1);
    }

    winAttribes.border_pixel = 0;
    winAttribes.border_pixmap = 0;
    winAttribes.background_pixmap = 0;
    winAttribes.colormap = XCreateColormap(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    gpXVisualInfo->visual, AllocNone);

    gColormap = winAttribes.colormap;
    winAttribes.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribes.event_mask = ExposureMask | VisibilityChangeMask |
    ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
    0, 0, giWindowWidth, giWindowHeight, 0, gpXVisualInfo->depth, InputOutput,
    gpXVisualInfo->visual, styleMask, &winAttribes);

    if(!gWindow)
    {
        printf("ERROR : Failed to create main window.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Triangle multiviewport!");
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = { 0 };

    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));
    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    False,
    StructureNotifyMask,
    &xev);
}

void uninitialize()
{
    GLXContext currentGlxContext = glXGetCurrentContext();

    if(currentGlxContext != NULL && currentGlxContext == gGlxContex)
    {
        glXMakeCurrent(gpDisplay, 0, 0);

        if(gGlxContex)
        {
            glXDestroyContext(gpDisplay, gGlxContex);
        }
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

void initialize(void)
{
    GLenum result;
    // method declarations

    gGlxContex = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

    glXMakeCurrent(gpDisplay, gWindow, gGlxContex);
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	// warm up call to resize
	resize(0, 0, giWindowWidth, giWindowHeight);
}

void resize(GLint x, GLint y, int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(x, y, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);

	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-1.0f, -1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2f(1.0f, -1.0f);
	glEnd();
	
	glXSwapBuffers(gpDisplay, gWindow);
}


