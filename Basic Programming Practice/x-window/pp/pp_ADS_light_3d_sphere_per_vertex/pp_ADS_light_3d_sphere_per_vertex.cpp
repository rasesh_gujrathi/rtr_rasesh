// g++ -o pp_ADS_light_3d_sphere_per_vertex.o pp_ADS_light_3d_sphere_per_vertex.cpp -L . -L/usr/lib/x86_64-linux-gnu -lstdc++  -lX11 -lGL -lGLEW -lGLU -lSphere

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <unistd.h>

// xlib headers
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

// opengl headers
#include <GL/glew.h>	// added for programmable pipeline
#include <GL/gl.h>
#include <GL/glx.h>
#include "vmath.h"
#include "Sphere.h"

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumVertices;
unsigned int gNumElements;

// namespaces
using namespace vmath;

bool bLight = true;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCORD
};

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;
mat4 modelMatrix;
mat4 viewMatrix;

GLint gVertexShaderObject;
GLint gFragmentShederObject;
GLint gShaderProgramObject;

bool gbActiveWindow = false;
bool gbFullScreen = false;

GLuint modelUniformMatrix;
GLuint viewUniformMatrix;
GLuint projectionMatrixUniform;

// light uniforms
GLuint laUniform;
GLuint lsUniform;
GLuint ldUniform;

// material/surface uniforms
GLuint kaUniform;
GLuint ksUniform;
GLuint kdUniform;

float materialShininess;
GLuint lightPositionUniform;
GLuint lKeyPressedUniform;

// global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
static GLXContext gGlxContext;

// entry point function
int main(void)
{
    // function prototypes
    void CreateWindow();
    void ToggleFullscreen(void);
    void uninitialize();
    void initialize(void);
    void resize(int, int);
    void display(void);

    // variable declarations
    static XFontStruct *pXFontStruct = NULL;
    static int winWidth = giWindowWidth;
    static int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;

    // code
    CreateWindow();

    initialize();

    // Message loop
    XEvent event;
    KeySym keysum;

    GC gc;
    XGCValues gcValues;
    XColor text_color;
    int strLength;
    int strWidth;
    int fontHeight;

    while(bDone == false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify: 
                pXFontStruct = XLoadQueryFont(gpDisplay, "fixed");
                break;
                case KeyPress: 
                keysum = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                switch(keysum)
                {
                    case XK_Escape: bDone = true;               
                    break;
                    default: break;                    
                }//switch
                XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                switch(keys[0])
                {
                    case 'L':
                    case 'l':
                        if (bLight == false)
                        {
                            bLight = true;
                        }
                        else
                        {
                            bLight = false;
                        }
                        break;
                    case 'F':
                    case 'f':
                        ToggleFullscreen();
                        if(bFullScreen == false)
                        {
                            bFullScreen = true;            
                        }
                        else
                        {
                            bFullScreen = false;            
                        }
                        break;
                }
                break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1: break;  // left
                        case 2: break;  // middle
                        case 3: break;  // right
                        case 4: break;  // mouse wheel up
                        case 5: break;  // mouse wheel down
                        default: break;
                    }
                break;
                case MotionNotify: break;
                case ConfigureNotify: 
                winWidth = event.xconfigure.width;
                winHeight = event.xconfigure.height;
                resize(winWidth, winHeight);
                break;
                case DestroyNotify:
                break;
                case 33:    // window cross ('x') pressed to close window
                XFreeGC(gpDisplay, gc);
                XUnloadFont(gpDisplay, pXFontStruct->fid);
                bDone = true;
                default: break;
            }//switch        
        }//while(xPending(gpDisplay))
        display();
    }//while(bDone == false)

    uninitialize();
    return 0;
}

typedef GLXContext (*GLXCreateContextAttribsARBProc)(Display*, GLXFBConfig,
 GLXContext, Bool, const int *);

GLXCreateContextAttribsARBProc glxCreateContextAttribsARB = NULL;

GLXFBConfig gGlxFBConfig;

void CreateWindow(void)
{
    // local variables
    GLXFBConfig *pGlxFBConfig = NULL;
    GLXFBConfig bestGLXFBConfig;
    XVisualInfo *ptempXVisualInfo;
    int iNumberOfFBConfigs = 0;

    // function prototype
    void uninitialize();

    // variable declarations
    XSetWindowAttributes winAttribes;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    static int frameBufferAttributes[] = 
    {
      GLX_X_RENDERABLE    , True,
      GLX_DRAWABLE_TYPE   , GLX_WINDOW_BIT,
      GLX_RENDER_TYPE     , GLX_RGBA_BIT,
      GLX_X_VISUAL_TYPE   , GLX_TRUE_COLOR,
      GLX_RED_SIZE        , 8,
      GLX_GREEN_SIZE      , 8,
      GLX_BLUE_SIZE       , 8,
      GLX_ALPHA_SIZE      , 8,
      GLX_DEPTH_SIZE      , 24,
      GLX_STENCIL_SIZE    , 8,
      GLX_DOUBLEBUFFER    , True,
      None
    };

    // code
    gpDisplay = XOpenDisplay(NULL);

    if(gpDisplay == NULL)
    {
        printf("Error : unable to open x display.\nExisting now...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    defaultDepth = XDefaultDepth(gpDisplay, defaultScreen);
    
    // Retrieve all FB configs that driver has
    pGlxFBConfig = glXChooseFBConfig(gpDisplay, defaultScreen, 
    frameBufferAttributes, &iNumberOfFBConfigs);
    printf("%d fbconfigs matching given attributes\n", iNumberOfFBConfigs);

    // variables
    int bestFrameBufferConfig = -1;
    int bestNumberOfSamples = -1;
    int worstFrameBufferConfig = -1;
    int worstNumberOfSamples = 999;

    for(int i = 0; i < iNumberOfFBConfigs; i++)
    {
        // for each obtained FBConfig get temporary visual info
        ptempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGlxFBConfig[i]);
        int sampleBuffers = 0;

        // used to check capability of making 2 calls below
        if(ptempXVisualInfo)
        {
            // get number of sample buffers from respective FBConfigs
            glXGetFBConfigAttrib(gpDisplay, pGlxFBConfig[i], 
            GLX_SAMPLE_BUFFERS, &sampleBuffers);

            int samples;
            // get number of samples from respective FBConfig
            glXGetFBConfigAttrib(gpDisplay, pGlxFBConfig[i], GLX_SAMPLES, &samples);

            // more the number of samples and sampleBuffers, more the
            // eligible FBConfig is so do the comparison

            if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)
            {
                bestFrameBufferConfig = i;
                bestNumberOfSamples = samples;
            }

            if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNumberOfSamples)
            {
                worstNumberOfSamples = samples;
                worstFrameBufferConfig = i;
            }
        }

        XFree(ptempXVisualInfo);
    }// for

    // Now assign the found best one
    bestGLXFBConfig = pGlxFBConfig[bestFrameBufferConfig];

    // Now assign the same base to global one
    gGlxFBConfig = bestGLXFBConfig;

    XFree(pGlxFBConfig);

    // Now get the best visual
    gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to get a visual.\n Exiting now...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    defaultDepth = XDefaultDepth(gpDisplay, defaultScreen);

    winAttribes.border_pixel = 0;
    winAttribes.border_pixmap = 0;
    winAttribes.background_pixmap = 0;
    winAttribes.colormap = XCreateColormap(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    gpXVisualInfo->visual, AllocNone);

    gColormap = winAttribes.colormap;
    winAttribes.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribes.event_mask = ExposureMask | VisibilityChangeMask |
    ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
    0, 0, giWindowWidth, giWindowHeight,
    0, gpXVisualInfo->depth, InputOutput,
    gpXVisualInfo->visual, styleMask, &winAttribes);

    if(!gWindow)
    {
        printf("ERROR : Failed to create main window.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Programmable pipeline - Defuse light on 3d rotating cube!");
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = { 0 };

    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));
    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    False,
    StructureNotifyMask,
    &xev);
}

void uninitialize()
{
    GLXContext currentGlxContext = glXGetCurrentContext();

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext)
    {
        glXMakeCurrent(gpDisplay, 0, 0);

        if(gGlxContext)
        {
            glXDestroyContext(gpDisplay, gGlxContext);
        }
    }

    if(gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if(gpXVisualInfo)
    {
        XFree(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShederObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShederObject);
	gFragmentShederObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
}

void initialize(void)
{
    GLenum result;
    // method declarations
	void resize(int, int);

    glxCreateContextAttribsARB = (GLXCreateContextAttribsARBProc)
    glXGetProcAddressARB((const GLubyte*)"glXCreateContextAttribsARB");

    if(glxCreateContextAttribsARB == NULL)
    {
        printf("Full h/w rendering context not available.\n");
        exit(0);
    }

    // Now get the context using context attribute array
    const GLint attribs[] = {GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
    GLX_CONTEXT_MINOR_VERSION_ARB, 5,
    GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB, 
    None};

    gGlxContext = glxCreateContextAttribsARB(gpDisplay, gGlxFBConfig,
                                             0, 
                                             True, 
                                             attribs);
    if(!gGlxContext)
    {
        // If failed to get the highest one, specify the lowest one
        // It will give you the highest one that is known or supported by 
        // the underlying rasterizer

        GLint attribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
        GLX_CONTEXT_MINOR_VERSION_ARB, 0, None};

        gGlxContext = glxCreateContextAttribsARB(gpDisplay, gGlxFBConfig, 
        0, True, attribs);
    }

    GLint attribs1[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
        GLX_CONTEXT_MINOR_VERSION_ARB, 0, None};

    // check whether the obtained context is really h/w rendering or not
    gGlxContext = glxCreateContextAttribsARB(gpDisplay, gGlxFBConfig, 0, True, attribs1);

    if(!glXIsDirect(gpDisplay, gGlxContext))
    {
        printf("Obtained context is NOT hardware rendering context...\n");
    }
    else
    {
        printf("Obtained context is hardware rendering context...\n");
    }

    if(glXMakeCurrent(gpDisplay, gWindow, gGlxContext) == True)
    {
        printf("Context set successful...\n");
    }
    else
    {
        printf("Context set unsuccessful...\n");
    }

	result = glewInit();	// enable all graphic library extensions i.e. core profile (only programmable pipeline)
	
	if (result != GLEW_OK)
	{
        fprintf(stderr, "Error: %s\n", glewGetErrorString(result));
		uninitialize();
        exit(1);
	}

    // define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// write vertex shader code
	const GLchar* vertexShaderSourceCode = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec4 u_light_position;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shininess;" \
		"out vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_cordinates = u_viewMatrix * u_modelMatrix * vPosition;" \
		"vec3 transformedNormal = normalize(mat3(u_viewMatrix * u_modelMatrix) * vNormal);" \
		"vec3 lightDirection = normalize(vec3(u_light_position) - eye_cordinates.xyz);" \
		"float transformed_dot_lightDirection = max(dot(lightDirection, transformedNormal), 0.0f);" \
		"vec3 reflection_vector = reflect(-lightDirection, transformedNormal);" \
		"vec3 viewer_vector = normalize(vec3(-eye_cordinates.xyz));" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 defuse = u_ld * u_kd * transformed_dot_lightDirection;" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, viewer_vector), 0.0f), u_material_shininess);" \
		"fong_ads_light = vec3(ambient + defuse + specular);" \
		"}" \
		"else" \
		"{" \
		"fong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar * *)& vertexShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLen = 0;
	char* szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// define fragment shader object
	gFragmentShederObject = glCreateShader(GL_FRAGMENT_SHADER);

	// fragment shader code
	const GLchar* fragmentShaderSourceCode = (const GLchar*)
		"#version 450 core" \
		"\n" \
		"in vec3 fong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(fong_ads_light, 1.0);" \
		"}";

	glShaderSource(gFragmentShederObject, 1, (const GLchar * *)& fragmentShaderSourceCode, NULL);

	// compile the vertex shader
	glCompileShader(gFragmentShederObject);

	iShaderCompileStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetShaderiv(gFragmentShederObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		// compilation failed
		glGetShaderiv(gFragmentShederObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShederObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Create shader program object
	gShaderProgramObject = glCreateProgram();

	// Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShederObject);

	// prelinking - binding to vertex attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link the shader program
	glLinkProgram(gShaderProgramObject);

	// error handling
	GLint iProgramLinkStatus = 0;
	iInfoLogLen = 0;
	if (szInfoLog)
		free(szInfoLog);

	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		// compilation failed
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLen);

		if (iInfoLogLen > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLen);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLogLen, &written, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	modelUniformMatrix = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininess = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	viewUniformMatrix = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();	

	// warm up call to resize
	resize(giWindowWidth, giWindowHeight);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	// do necessary transformation
	modelMatrix = translate(0.2f, 0.0f, -2.0f);

	// do matrix multiplication
	// TRS - transform, rotate, scale

	// send necessary matrices to shaders in respective uniforms
	// mvpUniform = glGetUniformLocation(...) 
	// so mvpUniform on CPU and u_mvp_matrix is on GPU
	// transpose means interchanging matrix row and column which is required in direct-x 
	// but not in open-gl so GL_FALSE
	glUniformMatrix4fv(modelUniformMatrix, 1 /* 1 array */, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniformMatrix, 1 /* 1 array */, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1 /* 1 array */, GL_FALSE, perspectiveProjectionMatrix);

	GLfloat lightPosition[4] = { 100.0f, 100.0f, 100.0f, 1.0f };
	GLfloat lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat lightAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	GLfloat lightDefuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat materialDefuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat materialAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	GLfloat materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat material_Shininess = 50.0f;		// try 128.0

	if (bLight == true)
	{
		glUniform1i(lKeyPressedUniform, 1);
		glUniform1f(materialShininess, material_Shininess);

		// light
		glUniform3fv(laUniform, 1, lightAmbient); 
		glUniform3fv(ldUniform, 1, lightDefuse); 
		glUniform3fv(lsUniform, 1, lightSpecular);

		// material
		glUniform3fv(kaUniform, 1, materialAmbient); 
		glUniform3fv(kdUniform, 1, materialDefuse); 
		glUniform3fv(ksUniform, 1, materialSpecular); 

		glUniform4fv(lightPositionUniform, 1, lightPosition); // equivalent to glLightfv in ffp
	}
	else
	{
		glUniform1i(lKeyPressedUniform, 0);
	}

	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// similarly bind with textures if any

	// draw necessary scene
	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	// unbind vao
	glBindVertexArray(0);

	glUseProgram(0);

	glXSwapBuffers(gpDisplay, gWindow);
}
