// g++ -o x-window-hello-window.o x-window-hello-window.cpp -L/usr/lib/x86_64-linux-gnu -lX11

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

// namespaces
using namespace std;

// global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

// entry point function
int main(void)
{
    // function prototypes
    void CreateWindow();
    void ToggleFullscreen(void);
    void uninitialize();
    static XFontStruct *pXFontStruct = NULL;
    // variable declarations
    static int winWidth = giWindowWidth;
    static int winHeight = giWindowHeight;
    char keys[26];
    // code
    CreateWindow();

    // Message loop
    XEvent event;
    KeySym keysum;

    GC gc;
    XGCValues gcValues;
    XColor text_color;
    char str[255] = "Hello World!!!";
    int strLength;
    int strWidth;
    int fontHeight;

    while(1)
    {
        XNextEvent(gpDisplay, &event);
        switch(event.type)
        {
            case MapNotify: 
            pXFontStruct = XLoadQueryFont(gpDisplay, "fixed");
            break;
            case KeyPress: 
            keysum = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
            switch(keysum)
            {
                case XK_Escape: uninitialize();
                                exit(0);                
                break;
                default: break;                    
            }//switch
            XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
            switch(keys[0])
            {
                case 'F':
                case 'f':
                    ToggleFullscreen();
                    if(bFullScreen == false)
                    {
                        bFullScreen = true;            
                    }
                    else
                    {
                        bFullScreen = false;            
                    }
                    break;
            }
            break;
            case ButtonPress:
                switch(event.xbutton.button)
                {
                    case 1: break;  // left
                    case 2: break;  // middle
                    case 3: break;  // right
                    case 4: break;  // mouse wheel up
                    case 5: break;  // mouse wheel down
                    default: break;
                }
            break;
            case MotionNotify: break;
            case ConfigureNotify: 
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            break;
            case Expose:
                gc = XCreateGC(gpDisplay, gWindow, 0, &gcValues);
                XSetFont(gpDisplay, gc, pXFontStruct->fid);
                XAllocNamedColor(gpDisplay, gColormap, "green", &text_color, &text_color);
                XSetForeground(gpDisplay, gc, text_color.pixel);
                strLength = strlen(str);
                // Find text length
                strWidth = XTextWidth(pXFontStruct, str, strLength);
                fontHeight = pXFontStruct->ascent + pXFontStruct->descent;
                XDrawString(gpDisplay, gWindow, gc, winWidth / 2 - strWidth / 2,
                winHeight / 2 - fontHeight / 2, str, strLength);
            break;
            case DestroyNotify:
            break;
            case 33:
            XFreeGC(gpDisplay, gc);
            XUnloadFont(gpDisplay, pXFontStruct->fid);
            uninitialize();
            exit(0);
            default: break;
        }//switch
    }

    uninitialize();
    return 0;
}

void CreateWindow(void)
{
    // function prototype
    void uninitialize();

    // variable declarations
    XSetWindowAttributes winAttribes;
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    // code
    gpDisplay = XOpenDisplay(NULL);

    if(gpDisplay == NULL)
    {
        printf("Error : unable to open x display.\nExisting now...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    defaultDepth = XDefaultDepth(gpDisplay, defaultScreen);
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: unable to allocate memory for visual info.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XMatchVisualInfo (gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo);

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to get a visual.\n Exiting now...\n");
        uninitialize();
        exit(1);
    }

    winAttribes.border_pixel = 0;
    winAttribes.border_pixmap = 0;
    winAttribes.background_pixmap = 0;
    winAttribes.colormap = XCreateColormap(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    gpXVisualInfo->visual, AllocNone);

    gColormap = winAttribes.colormap;
    winAttribes.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribes.event_mask = ExposureMask | VisibilityChangeMask |
    ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
    0, 0, giWindowWidth, giWindowHeight,
    0, gpXVisualInfo->depth, InputOutput,
    gpXVisualInfo->visual, styleMask, &winAttribes);

    if(!gWindow)
    {
        printf("ERROR : Failed to create main window.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "XWindow: Hello World!");
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = { 0 };

    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));
    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    False,
    StructureNotifyMask,
    &xev);
}

void uninitialize()
{
    if(gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}
