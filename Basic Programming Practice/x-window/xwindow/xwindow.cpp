// g++ -o xwindow.o xwindow.cpp -L/usr/lib/x86_64-linux-gnu -lX11

#include <iostream>     // Not a header, iosteam namespace cache
#include <stdio.h>      // standard i/o
#include <stdlib.h>     // exit
#include <memory.h>     // memset

#include <X11/Xlib.h>   // like windows.h on windows platform
#include <X11/Xutil.h>  // for XVisualInfo struct
#include <X11/XKBlib.h> // keyboard
#include <X11/keysym.h> // keyboard symbol mapping

// namespaces
using namespace std;    // part of iostream namespace

// global variable declarations
bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

// entry point function
int main(void)
{
    // function prototypes
    void CreateWindow();
    void ToggleFullscreen(void);
    void uninitialize();

    // variable declarations
    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

    // code
    CreateWindow();

    // Message loop
    XEvent event;
    KeySym keysum;

    while(1)
    {
        XNextEvent(gpDisplay, &event);  // similar to getmessage on Windows
        switch(event.type)
        {
            case MapNotify: // similar to WM_CREATE, comes only once when our window gets mapped to the default window created in background
            // unline windows, this shows the window as well, on windows we have to call showwindow() to show the window
            break;  
            case KeyPress: 
            keysum = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0 /* default code page ANSI*/, 0 /* Is shift key pressed? */);   
            switch(keysum)
            {
                case XK_Escape: uninitialize();
                                exit(0);
                case XK_F:
                case XK_f:
                    ToggleFullscreen();
                    if(bFullScreen == false)
                    {
                        bFullScreen = true;            
                    }
                    else
                    {
                        bFullScreen = false;            
                    }
                break;
                default: break;                    
            }//switch
            break;
            case ButtonPress:
                switch(event.xbutton.button)
                {
                    case 1: break;  // left
                    case 2: break;  // middle
                    case 3: break;  // right
                    case 4: break;  // mouse wheel up
                    case 5: break;  // mouse wheel down
                    default: break;
                }
            break;
            case MotionNotify: break;   // mouse move
            case ConfigureNotify:       // WM_SIZE
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            break;
            case Expose:                // WM_PAINT
            break;
            case DestroyNotify:         // WM_CLOSE
            break;
            case 33:                    // Window manager close window message
            uninitialize();
            exit(0);
            default: break;
        }//switch
    }

    uninitialize();
    return 0;
}

void CreateWindow(void)
{
    // function prototype
    void uninitialize();

    // variable declarations
    XSetWindowAttributes winAttribes;   // similar to WndClassex on windows
    int defaultScreen;
    int defaultDepth;
    int styleMask;

    // code
    gpDisplay = XOpenDisplay(NULL);     // opens the connection and GETs display

    if(gpDisplay == NULL)
    {
        printf("Error : unable to open x display.\nExisting now...\n");
        uninitialize();
        exit(1);
    }

    defaultScreen = /*XDefaultScreen or DefaultScreen*/ XDefaultScreen(gpDisplay); // get primary monitor
    defaultDepth = XDefaultDepth(gpDisplay, defaultScreen);
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR: unable to allocate memory for visual info.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XMatchVisualInfo (gpDisplay, defaultScreen, defaultDepth, TrueColor, gpXVisualInfo); // TrueColor = 32bit

    if(gpXVisualInfo == NULL)
    {
        printf("ERROR : Unable to get a visual.\n Exiting now...\n");
        uninitialize();
        exit(1);
    }

    winAttribes.border_pixel = 0;   // border image
    winAttribes.border_pixmap = 0;  // border color
    winAttribes.background_pixmap = 0;  // background image
    winAttribes.colormap = XCreateColormap(gpDisplay, // sessesion manager keeps check on gpDisplay i.e. valid and alive
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    gpXVisualInfo->visual, AllocNone);

    gColormap = winAttribes.colormap;
    winAttribes.background_pixel = BlackPixel(gpDisplay, defaultScreen);
    winAttribes.event_mask = ExposureMask | VisibilityChangeMask |
    ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
    0, 0, giWindowWidth, giWindowHeight,
    0, gpXVisualInfo->depth, InputOutput,
    gpXVisualInfo->visual, styleMask, &winAttribes);

    if(!gWindow)
    {
        printf("ERROR : Failed to create main window.\nExiting now...\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Rasesh: First XWindow");    // name the window
    Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);    // for close button and close menu
    XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
    XMapWindow(gpDisplay, gWindow); // map our created window with default one 
}

void ToggleFullscreen(void)
{
    // variable declarations
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = { 0 };

    // code
    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));
    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;    // 32 bit
    xev.xclient.data.l[0] = bFullScreen ? 0 : 1;
    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay, 
    RootWindow(gpDisplay, gpXVisualInfo->screen),
    False,  // non propagate-able
    StructureNotifyMask,    // event mask for structure change i.e. here size change 
    &xev);
}

// free the memory resources as used
void uninitialize()
{
    if(gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}
